package cz.cvut.fel.foodtracker.modules;

import android.app.Application;

import javax.inject.Singleton;

import cz.cvut.fel.foodtracker.FoodTrackerApplication;
import cz.cvut.fel.foodtracker.activities.AddFoodActivity;
import cz.cvut.fel.foodtracker.activities.FridgeContentActivity;
import cz.cvut.fel.foodtracker.activities.MainActivity;
import cz.cvut.fel.foodtracker.activities.SettingsActivity;
import cz.cvut.fel.foodtracker.activities.ShoppingListMenuActivity;
import cz.cvut.fel.foodtracker.activities.dialogs.NewCategoryOrStorageDialog;
import cz.cvut.fel.foodtracker.activities.dialogs.NumberPickerDialog;
import cz.cvut.fel.foodtracker.activities.dialogs.ProductSpecificationsDialog;
import cz.cvut.fel.foodtracker.activities.fragments.NewShoppingList;
import cz.cvut.fel.foodtracker.activities.fragments.ShoppingListDetailActivity;
import cz.cvut.fel.foodtracker.activities.fragments.ShoppingListHistory;
import cz.cvut.fel.foodtracker.activities.fragments.TimePickerFragment;
import cz.cvut.fel.foodtracker.services.ConfigurationService;
import cz.cvut.fel.foodtracker.services.NotificationService;
import cz.cvut.fel.foodtracker.services.ProductService;
import cz.cvut.fel.foodtracker.services.ShoppingListService;
import cz.cvut.fel.foodtracker.services.ifaces.ConfigurationServiceInterface;
import cz.cvut.fel.foodtracker.services.ifaces.ProductServiceInterface;
import cz.cvut.fel.foodtracker.services.ifaces.ShoppingListServiceInterface;
import dagger.Binds;
import dagger.Module;
import dagger.android.AndroidInjectionModule;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by vaicondr on 2/18/2018.
 */


@Module(includes = AndroidInjectionModule.class)
abstract class ApplicationContextModule {

    @Binds
    @Singleton
    abstract Application application(FoodTrackerApplication app);

    @ContributesAndroidInjector
    abstract MainActivity addInjector();

    @ContributesAndroidInjector
    abstract AddFoodActivity addFoodInjector();

    @ContributesAndroidInjector
    abstract FridgeContentActivity fridgeContentInjector();

    @ContributesAndroidInjector
    abstract ShoppingListMenuActivity shoppingListMenu();

    @ContributesAndroidInjector
    abstract ShoppingListHistory shoppingListHistory();

    @ContributesAndroidInjector
    abstract NewShoppingList newShoppingList();

    @ContributesAndroidInjector
    abstract SettingsActivity settingsActivity();

    @ContributesAndroidInjector
    abstract ProductSpecificationsDialog productSpecificationDialog();

    @ContributesAndroidInjector
    abstract NumberPickerDialog providesNumberPicker();

    @ContributesAndroidInjector
    abstract TimePickerFragment providesTimePicker();

    @ContributesAndroidInjector
    abstract NotificationService providesForNotificationService();

    @ContributesAndroidInjector
    abstract NewCategoryOrStorageDialog providesForNewCategoryDialog();

    @ContributesAndroidInjector
    abstract ShoppingListDetailActivity shoppingListDetailActivity();

    @Binds
    public abstract ProductServiceInterface providesProductService(ProductService productService);

    @Binds
    public abstract ShoppingListServiceInterface providesShoppingService(ShoppingListService shoppingListService);

    @Binds
    public abstract ConfigurationServiceInterface provideConfigurationService(ConfigurationService configurationService);

}
