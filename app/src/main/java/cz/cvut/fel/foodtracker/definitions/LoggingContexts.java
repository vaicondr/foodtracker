package cz.cvut.fel.foodtracker.definitions;

/**
 * Created by vaicondr on 4/14/2018.
 */

public class LoggingContexts {
    public static final String DATABASE = "DatabaseService";
    public static final String PRODUCT_SERVICE = "ProductService";

    public static final String ADAPTERS = "Adapters";
}
