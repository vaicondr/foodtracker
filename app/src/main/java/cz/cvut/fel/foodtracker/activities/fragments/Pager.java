package cz.cvut.fel.foodtracker.activities.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import cz.cvut.fel.foodtracker.controllers.ListItemsObserver;

/**
 * Created by vaicondr on 3/3/2018.
 */

public class Pager extends FragmentStatePagerAdapter {

    private int tabCount;

    private NewShoppingList newListTab;
    private ShoppingListHistory shoppingListHistory;

    public Pager(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;

        this.newListTab = new NewShoppingList();
        this.shoppingListHistory = new ShoppingListHistory();
        ListItemsObserver shoppingListSavedObserver = new ListItemsObserver(shoppingListHistory);
        newListTab.setShoppingListSavedObserver(shoppingListSavedObserver);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return newListTab;
            case 1:
                return shoppingListHistory;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
