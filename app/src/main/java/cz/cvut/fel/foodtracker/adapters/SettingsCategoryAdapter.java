package cz.cvut.fel.foodtracker.adapters;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.j256.ormlite.logger.Logger;
import com.j256.ormlite.logger.LoggerFactory;

import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import cz.cvut.fel.foodtracker.R;

import butterknife.BindView;
import cz.cvut.fel.foodtracker.definitions.CategorySettingsView;
import cz.cvut.fel.foodtracker.definitions.LoggingContexts;
import cz.cvut.fel.foodtracker.entities.ProductCategory;
import cz.cvut.fel.foodtracker.services.ifaces.ProductServiceInterface;
import dagger.android.AndroidInjection;
import dagger.android.support.AndroidSupportInjection;

/**
 * Created by endyw on 5/4/2018.
 */

public class SettingsCategoryAdapter extends BaseAdapter {

    final ProductServiceInterface productService;

    private List<CategorySettingsView> categorySettings;
    private Context context;
    Logger logger = LoggerFactory.getLogger(LoggingContexts.ADAPTERS);

    public SettingsCategoryAdapter(List<CategorySettingsView> categorySettings, Context context,
                                   ProductServiceInterface productService) {
        this.categorySettings = categorySettings;
        this.context = context;
        this.productService = productService;
    }

    static class ViewHolder {
        @BindView(R.id.settings_category_name) TextView categoryName;
        @BindView(R.id.time_limit) EditText timeLimit;
        int ref;

        ViewHolder(View view) {

            ButterKnife.bind(this, view);

        }
    }

    @Override
    public int getCount() {
        return categorySettings.size();
    }

    @Override
    public Object getItem(int position) {
        return categorySettings.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        LayoutInflater inflater = LayoutInflater.from(context);
        if(convertView == null) {
            convertView = inflater.inflate(R.layout.settings_row,null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.ref = position;
        CategorySettingsView cattegorySettingsView = categorySettings.get(position);
        if(cattegorySettingsView != null) {
            try {
                holder.categoryName.setText(cattegorySettingsView.getCategoryName());
                holder.timeLimit.setText(Integer.toString(cattegorySettingsView.getNotificationTimeInDays()));
                holder.timeLimit.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if(!s.toString().isEmpty()) {
                            categorySettings.get(holder.ref).setNotificationTimeInDays(Integer.parseInt(s.toString()));
                        }
                    }
                });
            } catch (Exception e) {
                logger.warn("Failed to update text views in settings adapter");
            }
        }
       return convertView;
    }

}
