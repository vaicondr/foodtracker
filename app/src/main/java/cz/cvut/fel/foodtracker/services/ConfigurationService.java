package cz.cvut.fel.foodtracker.services;


import java.util.List;

import javax.inject.Inject;

import cz.cvut.fel.foodtracker.dao.ConfigurationDao;
import cz.cvut.fel.foodtracker.definitions.ConfigurationType;
import cz.cvut.fel.foodtracker.entities.Configuration;
import cz.cvut.fel.foodtracker.services.ifaces.ConfigurationServiceInterface;

/**
 * Created by vaicondr on 5/3/2018.
 */

public class ConfigurationService implements ConfigurationServiceInterface {

    @Inject
    ConfigurationDao configurationDao;

    @Inject
    ConfigurationService() {

    }

    public ConfigurationService(ConfigurationDao configurationDao) {
        this.configurationDao = configurationDao;
    }

    @Override
    public void saveConfiguration(String configurationType, int configurationPayload) {
        Configuration foundConfiguration = findConfiguration(configurationType);
        if(foundConfiguration == null) {
            configurationDao.save(new Configuration(configurationType, configurationPayload));
        } else {
            foundConfiguration.setConfigurationPayload(configurationPayload);
            configurationDao.update(foundConfiguration);
        }
    }

    @Override
    public Configuration findConfiguration(String configurationType) {
        List<Configuration> configurations = configurationDao.findByConfigurationType(configurationType);
        if(configurations.isEmpty()) {
            return null;
        } else {
            return configurations.get(0);
        }
    }

}
