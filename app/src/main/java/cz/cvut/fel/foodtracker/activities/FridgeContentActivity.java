package cz.cvut.fel.foodtracker.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.internal.widget.AdapterViewCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.cvut.fel.foodtracker.R;
import cz.cvut.fel.foodtracker.activities.dialogs.ProductSpecificationsDialog;
import cz.cvut.fel.foodtracker.adapters.ProductListAdapter;
import cz.cvut.fel.foodtracker.controllers.ProductSpecificationDialogListener;
import cz.cvut.fel.foodtracker.controllers.Refreshable;
import cz.cvut.fel.foodtracker.definitions.BackendConstants;
import cz.cvut.fel.foodtracker.entities.Fridge;
import cz.cvut.fel.foodtracker.entities.ProductBranded;
import cz.cvut.fel.foodtracker.definitions.ProductView;
import cz.cvut.fel.foodtracker.entities.ProductCategory;
import cz.cvut.fel.foodtracker.entities.ProductType;
import cz.cvut.fel.foodtracker.entities.ProductUnit;
import cz.cvut.fel.foodtracker.services.ifaces.ProductServiceInterface;
import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

/**
 * Created by vaicondr on 3/3/2018.
 */

public class FridgeContentActivity extends AppCompatActivity implements HasSupportFragmentInjector,
        Refreshable,AdapterView.OnItemSelectedListener {

    List<ProductView> productsView;

    @Inject
    ProductSpecificationDialogListener productSpecificationDialogListener;

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    @Inject
    ProductServiceInterface productService;

    @BindView(R.id.fridge_empty_text)
    TextView fridgeEmptyText;

    @BindView(R.id.product_list)
    ListView productList;

    @BindView(R.id.expiration_sort_button)
    Button aboutToExpireSort;

    @BindView(R.id.fridge_spinner)
    Spinner fridgeSpinner;

    boolean filteredByExpiration = false;
    String fridgeSelected;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fridge_activity);
        AndroidInjection.inject(this);
        ButterKnife.bind(this);

        productSpecificationDialogListener.setObservedActivity(this);

        productsView = new ArrayList<>();

        productList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        productList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                ProductSpecificationsDialog productSpecificationsDialog = new ProductSpecificationsDialog();
                productSpecificationsDialog.setProductSpecificationDialogListener(productSpecificationDialogListener);

                ProductView productView = productsView.get(position);
                Bundle dialogBundle = new Bundle();


                dialogBundle.putString(BackendConstants.PRODUCT_NAME,productView.getTypeName());
                dialogBundle.putString(BackendConstants.PRODUCT_CATEGORY,productView.getCategoryName());
                dialogBundle.putInt(BackendConstants.PRODUCT_QUANTITY,productView.getQuantity());
                dialogBundle.putLong(BackendConstants.PRODUCT_EXPIRATION,productView.getExpirationDate());
                dialogBundle.putLong(BackendConstants.PRODUCT_ID,productView.getProductUnitId());
                dialogBundle.putString(BackendConstants.LOCATION,productView.getLocationStored());

                productSpecificationsDialog.setArguments(dialogBundle);
                productSpecificationsDialog.show(getSupportFragmentManager(),"");
            }
        });
        refreshListAdapter();
        createFridgeFilterSpinner();
    }

    @OnClick(R.id.expiration_sort_button)
    void sortByExpiration() {
        changeFilteredByExpiration();
        refreshListAdapter();
    }

    private void createFridgeFilterSpinner() {
        fridgeSpinner.setOnItemSelectedListener(this);
        List<Fridge> fridges = productService.findAllFridges();
        List<String> filterNames = new LinkedList<>();

        filterNames.add(BackendConstants.FRIDGE_UNFILTERED);
        filterNames.add(BackendConstants.NO_STORAGE);
        for(Fridge fridge : fridges) {
            filterNames.add(fridge.getFridgeName());
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item,
                filterNames.toArray(new String[filterNames.size()]));
        dataAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        fridgeSpinner.setAdapter(dataAdapter);
    }


    private void sortWithComparator(Comparator comparator) {
        Collections.sort(productsView, comparator);
        ProductListAdapter productListAdapter = new ProductListAdapter(getApplicationContext(),
                productsView,productService.findAllFridges());
        productList.setAdapter(productListAdapter);
    }

    private void createProductViewList(List<ProductUnit> products) {
        for(ProductUnit productUnit : products) {
                ProductType productType = productService.findType(productUnit.getProductTypeId());
                ProductCategory productCategory = productService.findCategory(productType.getProductCategoryId());
                Fridge fridge = productService.findFridge(productUnit.getFridgeId());
                String fridgeName;
                if (fridge == null) {
                    fridgeName = "";
                } else {
                    fridgeName = fridge.getFridgeName();
                }

                productsView.add(new ProductView(productUnit.getExpirationDate(),
                        productCategory.getCategoryName(),
                        productType.getProductName(),
                        productUnit.getQuantity(),
                        productUnit.getId(),
                        fridgeName,
                        productType.getUnits()));
        }
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    private List<ProductUnit> filterSelectionByFridge(String fridgeName) {
        List<ProductUnit> filteredProducts;
        if(fridgeName == null || fridgeName.equals(BackendConstants.FRIDGE_UNFILTERED )) {
            filteredProducts  = productService.findAllProductUnits();
        } else if(fridgeName.equals(BackendConstants.NO_STORAGE)){
            filteredProducts = productService.findProductsWithoutFridge();
        } else {
            Fridge fridge = productService.findFridgeByName(fridgeName);
            filteredProducts = productService.findProductsByFridge(fridge.getId());
        }

       return filteredProducts;
    }

    @Override
    public void refreshListAdapter() {
        productsView.clear();
        List<ProductUnit> products = filterSelectionByFridge(fridgeSelected);
        createProductViewList(products);
        if(productsView.isEmpty()) {
            fridgeEmptyText.setText(R.string.fridge_empty);
        } else {
            fridgeEmptyText.setText("");
        }
        if(filteredByExpiration) {
            sortWithComparator(new Comparator<ProductView>() {
                @Override
                public int compare(ProductView lhs, ProductView rhs) {
                    if(Math.abs(lhs.getExpirationDate() - System.currentTimeMillis()) <
                            Math.abs(rhs.getExpirationDate() - System.currentTimeMillis()) &&
                            lhs.getExpirationDate() > System.currentTimeMillis()) {
                        return -1;
                    } else if(Math.abs(lhs.getExpirationDate() - System.currentTimeMillis()) >
                            Math.abs(rhs.getExpirationDate() - System.currentTimeMillis()) &&
                            rhs.getExpirationDate() > System.currentTimeMillis()) {
                        return 1;
                    } else {
                        return 0;
                    }

                }
            });
        } else {
            ProductListAdapter productListAdapter = new ProductListAdapter(
                    getApplicationContext(), productsView, productService.findAllFridges());

            productList.setAdapter(productListAdapter);
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        fridgeSelected = parent.getItemAtPosition(position).toString();
        refreshListAdapter();
    }



    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void changeFilteredByExpiration() {
        if(filteredByExpiration) {
            filteredByExpiration = false;
        } else {
            filteredByExpiration = true;
        }
    }
}
