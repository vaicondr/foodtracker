package cz.cvut.fel.foodtracker.services;

import android.app.IntentService;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cz.cvut.fel.foodtracker.R;
import cz.cvut.fel.foodtracker.activities.FridgeContentActivity;
import cz.cvut.fel.foodtracker.definitions.BackendConstants;
import cz.cvut.fel.foodtracker.entities.ProductBranded;
import cz.cvut.fel.foodtracker.entities.ProductCategory;
import cz.cvut.fel.foodtracker.entities.ProductType;
import cz.cvut.fel.foodtracker.entities.ProductUnit;
import cz.cvut.fel.foodtracker.services.ifaces.ConfigurationServiceInterface;
import cz.cvut.fel.foodtracker.services.ifaces.ProductServiceInterface;
import dagger.android.AndroidInjection;

/**
 * Created by endyw on 5/3/2018.
 */

public class NotificationService extends IntentService {

    @Inject
    ProductServiceInterface productService;

    @Inject
    ConfigurationServiceInterface configurationService;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public NotificationService() {
        super(BackendConstants.SERVICE_THREAD_NAME);
    }

    @Override
    public void onCreate() {
        AndroidInjection.inject(this);
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if(intent != null) {
            checkForExpiringProducts();
        }
    }

    private void checkForExpiringProducts() {
        List<ProductUnit> productUnitList = productService.findAllProductUnits();
        List<String> productNamesToReport = new ArrayList<>();

        for(ProductUnit productUnit : productUnitList) {
            long currentTime = System.currentTimeMillis();
            ProductType productType = productService.findType(productUnit.getProductTypeId());
            ProductCategory productCategory = productService.findCategory(productType.getProductCategoryId());


            long categoryExpirationLimit = productCategory.getNotificationTime() * BackendConstants.DAY_IN_MS;
            if(productUnit.getExpirationDate() - currentTime < categoryExpirationLimit &&
                    productUnit.getExpirationDate() - currentTime > 0) {
                productNamesToReport.add(productType.getProductName());
            }
        }
        sendNot(productNamesToReport);
    }

    private void sendNot(List<String> expiringProducts) {
        if(!expiringProducts.isEmpty()) {
            Intent intent = new Intent(this, FridgeContentActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
            String bigText = createStringFromList(expiringProducts);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.foodtracker)
                    .setContentTitle(BackendConstants.NOTIFICATION_TITLE)
                    .setContentText(BackendConstants.NOTIFICATION_TEXT)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(bigText))
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);

            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
            int id = 91240;
            notificationManager.notify(id, mBuilder.build());
        }
    }

    private String createStringFromList(List<String> products) {
        String result = "";
        for(String string : products) {
            result += string + " ";
        }
        return result;
    }
}



