package cz.cvut.fel.foodtracker.activities.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.EditText;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.cvut.fel.foodtracker.R;
import cz.cvut.fel.foodtracker.controllers.Refreshable;
import cz.cvut.fel.foodtracker.services.ifaces.ProductServiceInterface;
import dagger.android.support.AndroidSupportInjection;

/**
 * Created by endyw on 5/4/2018.
 */

public class
NumberPickerDialog extends DialogFragment {

    @Inject
    ProductServiceInterface productService;

    @BindView(R.id.picked_number_global_limit)
    EditText pickedNumber;

    Refreshable settingsList;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = View.inflate(getActivity(),R.layout.number_picker_dialog,null);
        builder.setView(view);
        builder.setMessage(R.string.time_in_days)
                .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        int newLimit = Integer.parseInt(pickedNumber.getText().toString());

                        productService.updateGlobalLimit(newLimit);
                        settingsList.refreshListAdapter();
                    }
                })
                .setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        ButterKnife.bind(this,view);


        return builder.create();
    }

    public void setSettingsList(Refreshable settingsList) {
        this.settingsList = settingsList;
    }
}
