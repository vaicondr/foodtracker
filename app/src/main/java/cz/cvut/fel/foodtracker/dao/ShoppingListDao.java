package cz.cvut.fel.foodtracker.dao;

import java.util.List;

import javax.inject.Inject;

import cz.cvut.fel.foodtracker.entities.ShoppingList;
import cz.cvut.fel.foodtracker.helpers.DbHelper;

/**
 * Created by vaicondr on 4/16/2018.
 */

public class ShoppingListDao extends AbstractDAO<ShoppingList> {

    @Inject
    public ShoppingListDao(DbHelper dbHelper) {
        super(dbHelper.getConnectionSource());
    }

}
