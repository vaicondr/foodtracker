package cz.cvut.fel.foodtracker.services;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import cz.cvut.fel.foodtracker.dao.ProductTypeDao;
import cz.cvut.fel.foodtracker.dao.ProductUnitDao;
import cz.cvut.fel.foodtracker.dao.ShoppingListDao;
import cz.cvut.fel.foodtracker.dao.ShoppingListItemDao;
import cz.cvut.fel.foodtracker.definitions.UnitType;
import cz.cvut.fel.foodtracker.entities.ProductType;
import cz.cvut.fel.foodtracker.entities.ProductUnit;
import cz.cvut.fel.foodtracker.entities.ShoppingList;
import cz.cvut.fel.foodtracker.entities.ShoppingListItem;
import cz.cvut.fel.foodtracker.services.ifaces.ShoppingListServiceInterface;

/**
 * Created by vaicondr on 5/3/2018.
 */

public class ShoppingListService implements ShoppingListServiceInterface {

    @Inject
    ShoppingListDao shoppingListDao;

    @Inject
    ShoppingListItemDao shoppingListItemDao;

    @Inject
    ProductUnitDao productUnitDao;

    @Inject
    ProductTypeDao productTypeDao;

    @Inject
    public ShoppingListService() {

    }

    public ShoppingListService(ShoppingListDao shoppingListDao) {
        this.shoppingListDao = shoppingListDao;
    }

    @Override
    public ShoppingList findNewestShoppingList() {
        List<ShoppingList> shoppingLists = shoppingListDao.findAll();
        long max = 0;
        ShoppingList newestShoppingList = null;
        for(ShoppingList shoppingList : shoppingLists) {
            if(shoppingList.getDateCreated() > max) {
                newestShoppingList = shoppingList;
            }
        }
        return newestShoppingList;
    }

    @Override
    public void saveShoppingList(Map<String,Integer> productList) {
        shoppingListDao.save(new ShoppingList(System.currentTimeMillis()));
        ShoppingList shoppingList = findNewestShoppingList();

        for (Map.Entry<String,Integer> mapEntry : productList.entrySet()) {
            List<ProductType> productTypes = productTypeDao.findByProductTypeName(mapEntry.getKey());
            if(productTypes.isEmpty()) {
                productTypeDao.save(new ProductType(0,mapEntry.getKey(), UnitType.UNITS.toString()));
            }

            productTypes = productTypeDao.findByProductTypeName(mapEntry.getKey());
            shoppingListItemDao.save(new ShoppingListItem(productTypes.get(0).getId(),false,
                    mapEntry.getValue(),shoppingList.getId()));

        }

    }

    @Override
    public List<ShoppingListItem> findItemsByListId(long listId) {
        return shoppingListItemDao.findShoppingListItemsByListId(listId);
    }

    @Override
    public List<ShoppingListItem> findAllShoppingListItems() {
        return shoppingListItemDao.findAll();
    }

    @Override
    public void updateShoppingItem(ShoppingListItem shoppingListItem) {
        shoppingListItemDao.update(shoppingListItem);
    }

    @Override
    public void deleteShoppingItem(ShoppingListItem shoppingListItem) {
        shoppingListItemDao.delete(shoppingListItem);
    }

    @Override
    public List<ShoppingList> findAllShoppingLists() {
        return shoppingListDao.findAll();
    }
}
