package cz.cvut.fel.foodtracker.activities.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import javax.inject.Inject;

import cz.cvut.fel.foodtracker.R;
import cz.cvut.fel.foodtracker.controllers.AddFoodController;
import cz.cvut.fel.foodtracker.controllers.ProductSavedListener;
import cz.cvut.fel.foodtracker.controllers.ProductSpecificationDialogListener;
import cz.cvut.fel.foodtracker.definitions.BackendConstants;
import cz.cvut.fel.foodtracker.entities.ProductCategory;
import cz.cvut.fel.foodtracker.services.ifaces.ProductServiceInterface;
import dagger.android.support.AndroidSupportInjection;

/**
 * Created by endyw on 5/18/2018.
 */

public class NewCategoryOrStorageDialog extends DialogFragment {
    @Inject
    AddFoodController addFoodController;

    ProductSavedListener productSavedListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        final String productCategory = getArguments().getString(BackendConstants.PRODUCT_CATEGORY);
        final String productStorage = getArguments().getString(BackendConstants.LOCATION);

        boolean categoryExists = getArguments().getBoolean(BackendConstants.CATEGORY_EXISTS);
        boolean storageExists = getArguments().getBoolean(BackendConstants.STORAGE_EXISTS);


        StringBuilder messageBuilder = new StringBuilder();
        messageBuilder.append("By saving this product, you will create");

        if(!categoryExists) {
            messageBuilder.append(" new category " + productCategory + ",");
        }
        if(!storageExists) {
            messageBuilder.append(" new storage " + productStorage + ",");
        }

        messageBuilder.append(" continue?");
        String message = messageBuilder.toString();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final String productType = getArguments().getString(BackendConstants.PRODUCT_NAME);
        final String productCode = getArguments().getString(BackendConstants.PRODUCT_BARCODE_COLUMN);
        final String quantity = getArguments().getString(BackendConstants.PRODUCT_QUANTITY);
        final String selectedUnit = getArguments().getString(BackendConstants.UNITS);
        final long dateValue = getArguments().getLong(BackendConstants.DATE_VALUE);

        builder.setMessage(message)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        addFoodController.setDateValue(dateValue);
                        addFoodController.addProduct(productCategory,productType,productCode,quantity,productStorage,selectedUnit);
                        productSavedListener.onProductSaved();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        return builder.create();
    }

    public void setProductSavedListener(ProductSavedListener productSavedListener) {
        this.productSavedListener = productSavedListener;
    }
}
