package cz.cvut.fel.foodtracker.activities.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Date;


import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.cvut.fel.foodtracker.R;
import cz.cvut.fel.foodtracker.controllers.ProductSpecificationDialogListener;
import cz.cvut.fel.foodtracker.definitions.BackendConstants;
import cz.cvut.fel.foodtracker.entities.ProductType;
import cz.cvut.fel.foodtracker.helpers.DateStringParser;
import cz.cvut.fel.foodtracker.services.ifaces.ProductServiceInterface;
import dagger.android.support.AndroidSupportInjection;

/**
 * Created by vaicondr on 4/25/2018.
 */

public class ProductSpecificationsDialog extends DialogFragment {

    ProductSpecificationDialogListener productSpecificationDialogListener;

    @Inject
    ProductServiceInterface productService;

    @BindView(R.id.product_detail_expiration)
    TextView expiration;

    @BindView(R.id.product_detail_category)
    TextView category;

    @BindView(R.id.product_detail_quantity)
    EditText quantity;

    @BindView(R.id.product_detail_type)
    TextView productType;

    @BindView(R.id.product_storage_detail)
    TextView storage;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final String productName = getArguments().getString(BackendConstants.PRODUCT_NAME);
        final String productCategory = getArguments().getString(BackendConstants.PRODUCT_CATEGORY);
        final int productQuantity = getArguments().getInt(BackendConstants.PRODUCT_QUANTITY);
        long expirationDate = getArguments().getLong(BackendConstants.PRODUCT_EXPIRATION);
        final long productUnitId = getArguments().getLong(BackendConstants.PRODUCT_ID);
        String storageName = getArguments().getString(BackendConstants.LOCATION);


        View view = View.inflate(getActivity(),R.layout.product_detail_dialog,null);
        builder.setView(view)
                .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        int newQuantity = Integer.parseInt(quantity.getText().toString());
                        if(newQuantity != productQuantity) {
                            productSpecificationDialogListener.onProductQuantityUpdate(productUnitId,newQuantity);
                        }

                        String newCategory = category.getText().toString();
                        if(!newCategory.equals(productCategory)) {
                            ProductType originalProductType = productService.findTypeByName(productName);
                            productSpecificationDialogListener.onProductTypeUpdate(originalProductType.getId()
                                    ,newCategory);
                        }
                    }
                })
                .setNegativeButton(R.string.delete, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        AlertDeleteDialog alertDeleteDialog = new AlertDeleteDialog();
                        Bundle bundle = new Bundle();
                        bundle.putLong(BackendConstants.PRODUCT_ID,productUnitId);
                        alertDeleteDialog.setProductSpecificationDialogListener(productSpecificationDialogListener);
                        alertDeleteDialog.setArguments(bundle);
                        alertDeleteDialog.show(getFragmentManager(),getTag());
                    }
                })
                .setNeutralButton(R.string.close, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        ButterKnife.bind(this,view);

        productType.setText(productName);
        category.setText(productCategory);
        quantity.setText(Integer.toString(productQuantity));
        expiration.setText(DateStringParser.getOnlyDateAndYear(new Date(expirationDate).toString()));
        storage.setText(storageName);

        return builder.create();
    }

    public void setProductSpecificationDialogListener(ProductSpecificationDialogListener productSpecificationDialogListener) {
        this.productSpecificationDialogListener = productSpecificationDialogListener;
    }
}
