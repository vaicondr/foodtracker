package cz.cvut.fel.foodtracker.controllers;


/**
 * Created by vaicondr on 4/28/2018.
 */

public class ListItemsObserver {

    private Refreshable refreshable;

    public ListItemsObserver(Refreshable refreshable) {
        this.refreshable = refreshable;
    }

    public void onRefreshableListChanged() {
        refreshable.refreshListAdapter();
    }
}
