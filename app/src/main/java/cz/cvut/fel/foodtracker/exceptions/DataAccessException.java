package cz.cvut.fel.foodtracker.exceptions;

/**
 * Thrown when error accessing data occurs.
 * Should be used as a wrapper for any SQLException.
 *
 * @author balikm1
 * @since 0.4
 */
public class DataAccessException extends RuntimeException {

    public DataAccessException() {
    }

    public DataAccessException(String message) {
        super(message);
    }

    public DataAccessException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataAccessException(Throwable cause) {
        super(cause);
    }

}
