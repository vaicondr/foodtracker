package cz.cvut.fel.foodtracker.entities;

import javax.persistence.Column;
import javax.persistence.Entity;

import cz.cvut.fel.foodtracker.dao.AbstractPersistable;
import cz.cvut.fel.foodtracker.definitions.BackendConstants;

/**
 * Created by vaicondr on 4/4/2018.
 */


@Entity(name = BackendConstants.PRODUCT_UNIT_ENTITY)
public class ProductUnit extends AbstractPersistable {

    private static final long serialVersionUID = 1L;

    @Column(name = BackendConstants.PRODUCT_QUANTITY_COLUMN,nullable = false)
    int quantity;

    @Column(name = BackendConstants.PRODUCT_EXPIRATION_COLUMN)
    long expirationDate;

    @Column(name = BackendConstants.PRODUCT_TYPE_ID_COLUMN,nullable = false)
    long productTypeId;

    @Column(name = BackendConstants.PRODUCT_LOCATION_STORED_ID)
    long fridgeId;


    public ProductUnit() {
        this(0,0,0,0);
    }


    public ProductUnit(int quantity,long expirationDate,long fridgeId,long productTypeId) {
        this.quantity = quantity;
        this.expirationDate = expirationDate;
        this.fridgeId = fridgeId;
        this.productTypeId = productTypeId;
    }

    public int getQuantity() {
        return quantity;
    }

    public long getExpirationDate() {
        return expirationDate;
    }

    public long getFridgeId() {
        return fridgeId;
    }

    public long getProductTypeId() {
        return productTypeId;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setProductTypeId(long productTypeId) {
        this.productTypeId = productTypeId;
    }
}
