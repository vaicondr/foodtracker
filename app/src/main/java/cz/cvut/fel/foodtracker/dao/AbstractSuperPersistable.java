package cz.cvut.fel.foodtracker.dao;

import java.io.Serializable;
import java.util.UUID;

/**
 * Abstract parent class for JPA entities.
 * This class does not define the id field and generation strategy. Subclass this class to define your own id generator.
 * This is an alternative to the cz.cvut.fel.asf.persistence.AbstractSuperPersistable
 * without the XmlTransient annotation, which is not available for Android.
 * 
 * @author balikm1
 * @since 0.4
 */
 
public abstract class AbstractSuperPersistable<PK extends Serializable> implements IPersistable<PK> {

    private static final long serialVersionUID = -5554308939380869754L;
    
    private UUID t_id;
    
    /**
     * @deprecated This constructor is intended for internal use only, do not use it to instantiate the entity.
     */
    @Deprecated
    public AbstractSuperPersistable() { }
    
    public AbstractSuperPersistable(boolean creating) {
        t_id = UUID.randomUUID();
    }

    @Override
    abstract public PK getId();
   
    abstract protected void setId(final PK id);

    @Override
    public String toString() {
        return String.format("Entity of type %s with id: %s", this.getClass().getName(), getId());
    }
    
    /**
     * Method checks if the type of obj instance has the same type and can be compared by equals method
     * @param obj
     * @return true if obj can be equal to this instance
     */
    abstract protected boolean isTypeEquivalentTo(Object obj);

    @Override
    public boolean equals(Object obj) {
        if (this.t_id == null && this.getId() == null) {
            throw new UnsupportedOperationException("Unidentified entity - operation not supported! Do not use the default constructor.");
        }
        
        if (obj == null) {
            return false;
        }
        if (!isTypeEquivalentTo(obj)) {
            return false;
        }
        final AbstractSuperPersistable<?> other = (AbstractSuperPersistable<?>) obj;
        if (t_id != null && t_id.equals(other.t_id)) {
            return true;
        }
        if (this.getId() != null && this.getId().equals(other.getId())) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        if (this.t_id == null && this.getId() == null) {
            throw new UnsupportedOperationException("Unidentified entity - operation not supported! Do not use the default constructor.");
        }
        
        int hash = 7;
        hash = 89 * hash + (this.t_id != null ? this.t_id.hashCode() : this.getId().hashCode());
        return hash;
    }
}
