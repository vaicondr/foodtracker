package cz.cvut.fel.foodtracker.controllers;

/**
 * Created by vaicondr on 4/29/2018.
 */


/**
 * Serves as an interface for Activities with ListViews that need to me refreshed after certain actions
 */
public interface Refreshable {

    void refreshListAdapter();
}
