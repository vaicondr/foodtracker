package cz.cvut.fel.foodtracker.activities.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;


import cz.cvut.fel.foodtracker.R;
import cz.cvut.fel.foodtracker.controllers.ProductSpecificationDialogListener;
import cz.cvut.fel.foodtracker.definitions.BackendConstants;

/**
 * Created by vaicondr on 4/28/2018.
 */

public class AlertDeleteDialog extends DialogFragment {

    ProductSpecificationDialogListener productSpecificationDialogListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.alert_deleting)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        productSpecificationDialogListener.onProductDelete(getArguments().getLong(BackendConstants.PRODUCT_ID));
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        return builder.create();
    }

    public void setProductSpecificationDialogListener(ProductSpecificationDialogListener productSpecificationDialogListener) {
        this.productSpecificationDialogListener = productSpecificationDialogListener;
    }
}
