package cz.cvut.fel.foodtracker.definitions;

/**
 * Created by endyw on 5/3/2018.
 */

public abstract class BackendConstants {
    public static final String PRODUCT_NAME = "PRODUCT_NAME";
    public static final String PRODUCT_CATEGORY = "PRODUCT_CATEGORY";
    public static final String PRODUCT_QUANTITY = "PRODUCT_QUANTITY";
    public static final String PRODUCT_EXPIRATION = "PRODUCT_EXPIRATION";
    public static final String PRODUCT_ID = "PRODUCT_ID";
    public static final String LOCATION = "LOCATION";

    public static final String CATEGORY_EXISTS = "CATEGORY_EXISTS";
    public static final String STORAGE_EXISTS = "STORAGE_EXISTS";

    public static final String LIST_ITEMS = "LIST_ITEMS";

    public static final String LIST_PREVIEW = "List preview";
    public static final String LIST_POPULAR = "Popular";

    public static final String EMPTY_TEXT_VIEW = "";


    //TAGS
    public static final String SHOPPING_LIST_DETAIL_TAG = "SHOPPING_LIST_DETAIL_DIALOG";
    public static final String TIME_PICKER_TAG = "TIME_PICKER";
    public static final String NUMBER_PICKER_TAG = "NUMBER_PICKER";


    //PRODUCT BRANDED
    public static final String PRODUCT_BRANDED_ENTITY = "ProductBranded";
    public static final String PRODUCT_BARCODE_COLUMN = "productBarcode";
    public static final String PRODUCT_TYPE_ID_COLUMN = "productTypeId";

    //PRODUCT CATEGORY
    public static final String PRODUCT_CATEGORY_ENTITY = "ProductCategory";
    public static final String PRODUCT_CATEGORY_NAME_COLUMN = "productCategoryName";
    public static final String PRODUCT_CATEGORY_EXPIRATION_COLUMN = "expirationNotificationTime";

    //PRODUCT TYPE
    public static final String PRODUCT_TYPE_ENTITY = "ProductType";
    public static final String PRODUCT_CATEGORY_ID_COLUMN = "productCategoryId";
    public static final String PRODUCT_TYPE_NAME_COLUMN = "productName";
    public static final String UNITS = "productUnits";

    //PRODUCT UNIT
    public static final String PRODUCT_UNIT_ENTITY = "ProductUnit";
    public static final String PRODUCT_QUANTITY_COLUMN = "quantity";
    public static final String PRODUCT_EXPIRATION_COLUMN = "expirationDate";
    public static final String PRODUCT_LOCATION_STORED_ID = "locationStoredId";

    //SHOPPING LIST
    public static final String SHOPPING_LIST_ENTITY = "ShoppingList";
    public static final String SHOPPING_DATE_CREATED_COLUMN = "dateCreated";

    public static final String SHOPPING_LIST_ITEM_ENTITY = "ShoppingListItem";
    public static final String SHOPPING_LIST_ID_COLUMN = "shoppingListId";
    public static final String SHOPPING_LIST_QUANTITY_COLUMN = "itemQuantity";
    public static final String SHOPPING_ITEM_SAVED_COLUMN = "productSaved";


    //CONFIGURATION
    public static final String CONFIGURATION_ENTITY = "Configuration";
    public static final String CONFIGURATION_TYPE_COLUMN = "configurationType";
    public static final String CONFIGURATION_PAYLOAD_COLUMN = "configurationPayload";

    //FRIDGE
    public static final String FRIDGE_ENTITY = "Fridge";
    public static final String FRIDGE_NAME_COLUMN = "FridgeName";

    //NOTIFICATIONS
    public static final String NOTIFICATION_TITLE = "Expiration alert!";
    public static final String NOTIFICATION_TEXT = "There are some products that will soon expire!";

    public static final String SERVICE_THREAD_NAME = "NotificationServiceFT";
    public static final long DAY_IN_MS = 86400000;

    public static final String FRIDGE_UNFILTERED = "Unfiltered";
    public static final String NO_STORAGE = "No storage";

    public static final String POSITION = "Position";
    public static final String DATE_VALUE = "DateValue";

    public static final int ADD_FOOD_REQUEST_CODE = 1243;
}
