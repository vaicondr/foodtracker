package cz.cvut.fel.foodtracker.services.ifaces;

import java.util.List;
import java.util.Map;

import cz.cvut.fel.foodtracker.entities.ProductUnit;
import cz.cvut.fel.foodtracker.entities.ShoppingList;
import cz.cvut.fel.foodtracker.entities.ShoppingListItem;

/**
 * Created by endyw on 5/3/2018.
 */

public interface ShoppingListServiceInterface {
    ShoppingList findNewestShoppingList();

    void saveShoppingList(Map<String,Integer> productList);

    List<ShoppingListItem> findItemsByListId(long listId);

    List<ShoppingListItem> findAllShoppingListItems();

    void updateShoppingItem(ShoppingListItem shoppingListItem);

    void deleteShoppingItem(ShoppingListItem shoppingListItem);

    List<ShoppingList> findAllShoppingLists();
}
