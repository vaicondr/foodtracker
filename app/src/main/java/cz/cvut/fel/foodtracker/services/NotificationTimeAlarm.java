package cz.cvut.fel.foodtracker.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;

import javax.inject.Inject;

import cz.cvut.fel.foodtracker.definitions.ConfigurationPayload;
import cz.cvut.fel.foodtracker.definitions.ConfigurationType;
import cz.cvut.fel.foodtracker.services.ifaces.ConfigurationServiceInterface;

/**
 * Created by endyw on 5/5/2018.
 */

public class NotificationTimeAlarm {
    private AlarmManager alarmManager;
    private PendingIntent pendingIntent;

    @Inject
    ConfigurationServiceInterface configurationService;

    @Inject
    public NotificationTimeAlarm() {

    }

    public void setRepeatingAlarm(Context context,int hourOfDay,int minute) {

        boolean notificationsAllowed = configurationService.findConfiguration(ConfigurationType.NOTIFICATIONS_ALLOWED)
                .getConfigurationPayload() == ConfigurationPayload.notificationsAllowed;
        if(notificationsAllowed) {
            this.alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

            alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(context, NotificationTimeReceiver.class);
            pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);


            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendar.set(Calendar.MINUTE, minute);

            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                    AlarmManager.INTERVAL_DAY, pendingIntent);
        }
    }
}
