package cz.cvut.fel.foodtracker.definitions;

/**
 * Created by endyw on 5/19/2018.
 */

public enum  UnitType {
    UNITS ("pcs"),
    GRAMS ("g"),
    KILOGRAMS ("kg"),
    LITRES ("l"),
    MILILITRES ("ml");

    private final String name;

    UnitType(String s) {
        name = s;
    }

    public String toString() {
        return this.name;
    }

}
