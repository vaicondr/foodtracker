package cz.cvut.fel.foodtracker.dao;


import java.util.List;

import javax.inject.Inject;

import cz.cvut.fel.foodtracker.definitions.BackendConstants;
import cz.cvut.fel.foodtracker.entities.Fridge;
import cz.cvut.fel.foodtracker.helpers.DbHelper;

/**
 * Created by endyw on 5/16/2018.
 */

public class FridgeDao extends AbstractDAO<Fridge> {

    @Inject
    public FridgeDao(DbHelper dbHelper) {
        super(dbHelper.getConnectionSource());
    }

    public List<Fridge> findByFridgeName(String fridgeName) {
        return this.queryForEq(BackendConstants.FRIDGE_NAME_COLUMN,fridgeName);
    }
}
