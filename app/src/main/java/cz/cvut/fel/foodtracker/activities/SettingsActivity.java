package cz.cvut.fel.foodtracker.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import cz.cvut.fel.foodtracker.R;
import cz.cvut.fel.foodtracker.activities.dialogs.NumberPickerDialog;
import cz.cvut.fel.foodtracker.activities.fragments.TimePickerFragment;
import cz.cvut.fel.foodtracker.adapters.SettingsCategoryAdapter;
import cz.cvut.fel.foodtracker.controllers.Refreshable;
import cz.cvut.fel.foodtracker.definitions.BackendConstants;
import cz.cvut.fel.foodtracker.definitions.CategorySettingsView;
import cz.cvut.fel.foodtracker.definitions.ConfigurationPayload;
import cz.cvut.fel.foodtracker.definitions.ConfigurationType;
import cz.cvut.fel.foodtracker.entities.Configuration;
import cz.cvut.fel.foodtracker.entities.ProductCategory;
import cz.cvut.fel.foodtracker.services.ifaces.ConfigurationServiceInterface;
import cz.cvut.fel.foodtracker.services.ifaces.ProductServiceInterface;
import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

/**
 * Created by vaicondr on 4/29/2018.
 */

public class SettingsActivity extends AppCompatActivity implements HasSupportFragmentInjector,Refreshable {
    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    @Inject
    ConfigurationServiceInterface configurationService;

    @Inject
    ProductServiceInterface productService;

    @BindView(R.id.not_allowed)
    CheckBox notAllowedCheckbox;

    @BindView(R.id.choose_time_button)
    Button chooseTimeButton;

    @BindView(R.id.global_limit_button)
    Button setGlobalLimitButton;

    @BindView(R.id.settings_category_list)
    ListView settingsCategoryList;

    @BindView(R.id.category_empty)
    TextView emptyCategories;

    @BindView(R.id.save_config_button)
    Button saveConfigButton;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        AndroidInjection.inject(this);
        ButterKnife.bind(this);
        refreshListAdapter();
        checkCheckbox();
    }



    @OnClick(R.id.choose_time_button)
    void chooseTime() {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), BackendConstants.TIME_PICKER_TAG);
    }

    @OnClick(R.id.global_limit_button)
    void chooseGlobalLimit() {
        NumberPickerDialog dialog = new NumberPickerDialog();
        dialog.setSettingsList(this);
        dialog.show(getSupportFragmentManager(),BackendConstants.NUMBER_PICKER_TAG);
    }

    @OnClick(R.id.save_config_button)
    void saveConfig() {
        List<ProductCategory> listHeader = productService.findAllProductCategories();
        int listSize = settingsCategoryList.getAdapter().getCount();
        for (int i = 0; i <listSize ; i++) {
            CategorySettingsView settingsView = (CategorySettingsView)settingsCategoryList.getAdapter().getItem(i);
            if(settingsView.getNotificationTimeInDays() != listHeader.get(i).getNotificationTime()) {
                productService.updateCategoryLimit(listHeader.get(i),settingsView.getNotificationTimeInDays());
            }
        }

        Toast.makeText(this, R.string.conf_saved,
                Toast.LENGTH_LONG).show();
    }

    @OnCheckedChanged(R.id.not_allowed)
    void onAllowedNotifications(boolean isChecked) {
        int configurationPayload = isChecked ?  ConfigurationPayload.notificationsAllowed :
                ConfigurationPayload.notificationsAllowed;
        configurationService.saveConfiguration(ConfigurationType.NOTIFICATIONS_ALLOWED,configurationPayload);
    }


    @Override
    public void refreshListAdapter() {
        List<ProductCategory> productCategoryList = productService.findAllProductCategories();
        List<CategorySettingsView> categorySettingsList = new LinkedList<>();

        for(ProductCategory category : productCategoryList) {
            CategorySettingsView categorySettingsView = new CategorySettingsView(category.getCategoryName()
                    ,category.getNotificationTime());
            categorySettingsList.add(categorySettingsView);
        }
        if(categorySettingsList.isEmpty()) {
            emptyCategories.setText(R.string.no_categories);
        } else {
            SettingsCategoryAdapter settingsCategoryAdapter =
                    new SettingsCategoryAdapter(categorySettingsList,this,productService);
            settingsCategoryList.setAdapter(settingsCategoryAdapter);
        }

    }

    private void checkCheckbox() {
        Configuration configuration = configurationService.findConfiguration(ConfigurationType.NOTIFICATIONS_ALLOWED);
        if(configuration == null) {
            return;
        }
        int conf = configuration.getConfigurationPayload();
        boolean shouldBeChecked = conf == ConfigurationPayload.notificationsAllowed;

        notAllowedCheckbox.setChecked(shouldBeChecked);

    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

}
