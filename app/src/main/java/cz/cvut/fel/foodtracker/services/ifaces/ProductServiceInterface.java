package cz.cvut.fel.foodtracker.services.ifaces;

import java.util.List;

import cz.cvut.fel.foodtracker.entities.Fridge;
import cz.cvut.fel.foodtracker.entities.ProductBranded;
import cz.cvut.fel.foodtracker.entities.ProductCategory;
import cz.cvut.fel.foodtracker.entities.ProductType;
import cz.cvut.fel.foodtracker.entities.ProductUnit;

/**
 * Created by endyw on 5/3/2018.
 */

public interface ProductServiceInterface {
    void updateBranded(ProductBranded toUpdate);

    void updateGlobalLimit(int newLimit);

    void updateCategoryLimit(ProductCategory productCategory,int newLimit);

    void updateProductUnit(ProductUnit productUnit);

    void saveBranded(ProductBranded productBranded);

    void saveType(ProductType productType);

    void saveCategory(ProductCategory productCategory);

    void saveProductUnit(ProductUnit productUnit);

    void deleteProductUnit(ProductUnit productUnit);

    void deleteProductBranded(ProductBranded productBranded);

    ProductBranded findBrandedProductByBarcode(String barcode);

    ProductCategory findCategoryByName(String name);

    ProductType findTypeByName(String name);

    ProductUnit findUnitByTypeIdExpirationDateAndStorageId(long expirationDate,long typeId,long storageId);

    ProductCategory findCategory(long id);

    ProductType findType(long id);

    Fridge findFridge(long id);

    ProductBranded findBranded(long id);

    ProductUnit findUnit(long id);

    List<ProductUnit> findAllProductUnits();

    List<ProductType> findAllProductTypes();

    List<ProductCategory> findAllProductCategories();

    Fridge findLargestFridge();

    Fridge findFridgeByName(String fridgeName);

    void saveFridge(String fridgeName);

    List<Fridge> findAllFridges();

    void updateProductType(ProductType productType);

    void deleteProductType(ProductType productType);

    List<ProductUnit> findProductsByFridge(long fridgeId);

    List<ProductUnit> findProductsWithoutFridge();
}
