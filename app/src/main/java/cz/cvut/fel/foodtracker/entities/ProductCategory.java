package cz.cvut.fel.foodtracker.entities;

import javax.persistence.Column;
import javax.persistence.Entity;

import cz.cvut.fel.foodtracker.dao.AbstractPersistable;
import cz.cvut.fel.foodtracker.definitions.BackendConstants;

/**
 * Created by vaicondr on 3/18/2018.
 */

@Entity(name = BackendConstants.PRODUCT_CATEGORY_ENTITY)
public class ProductCategory extends AbstractPersistable {

    private static final long serialVersionUID = 1L;

    @Column(name = BackendConstants.PRODUCT_CATEGORY_NAME_COLUMN, nullable = false)
    private String categoryName;

    @Column(name = BackendConstants.PRODUCT_CATEGORY_EXPIRATION_COLUMN)
    private int notificationTime;

    public ProductCategory() {
        this("",0);
    }

    public ProductCategory(String categoryName,int notificationTime) {
        this.categoryName = categoryName;
        this.notificationTime = notificationTime;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public int getNotificationTime() {
        return notificationTime;
    }

    public void setNotificationTime(int notificationTime) {
        this.notificationTime = notificationTime;
    }
}