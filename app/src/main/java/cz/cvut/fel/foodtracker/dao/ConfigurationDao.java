package cz.cvut.fel.foodtracker.dao;



import java.util.List;

import javax.inject.Inject;

import cz.cvut.fel.foodtracker.definitions.BackendConstants;
import cz.cvut.fel.foodtracker.entities.Configuration;
import cz.cvut.fel.foodtracker.helpers.DbHelper;

/**
 * Created by endyw on 5/4/2018.
 */

public class ConfigurationDao extends AbstractDAO<Configuration> {

    @Inject
    public ConfigurationDao(DbHelper dbHelper) {
        super(dbHelper.getConnectionSource());
    }

    public List<Configuration> findByConfigurationType(String configurationType) {
        return queryForEq(BackendConstants.CONFIGURATION_TYPE_COLUMN,configurationType);
    }
}
