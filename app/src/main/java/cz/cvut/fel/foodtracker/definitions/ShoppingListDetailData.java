package cz.cvut.fel.foodtracker.definitions;

/**
 * Created by endyw on 5/19/2018.
 */

public class ShoppingListDetailData {
    private final String productName;
    private final int productQuantity;
    private final boolean productSaved;
    private final String units;

    public ShoppingListDetailData(String productName, int productQuantity, boolean productSaved,String units) {
        this.productName = productName;
        this.productQuantity = productQuantity;
        this.productSaved = productSaved;
        this.units = units;
    }

    public String getUnits() {
        return units;
    }

    public String getProductName() {
        return productName;
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public boolean isProductSaved() {
        return productSaved;
    }
}
