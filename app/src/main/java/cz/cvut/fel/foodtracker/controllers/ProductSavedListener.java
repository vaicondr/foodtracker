package cz.cvut.fel.foodtracker.controllers;

/**
 * Created by endyw on 5/19/2018.
 */

public interface ProductSavedListener {
    void onProductSaved();
}
