package cz.cvut.fel.foodtracker.modules;

/**
 * Created by vaicondr on 3/24/2018.
 */

import android.app.Activity;
import android.content.Context;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class BaseActivityModule {
    @Binds
    abstract Context activityContext(Activity activity);
}
