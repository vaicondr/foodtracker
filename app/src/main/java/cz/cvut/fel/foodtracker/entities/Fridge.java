package cz.cvut.fel.foodtracker.entities;

import javax.persistence.Column;
import javax.persistence.Entity;

import cz.cvut.fel.foodtracker.dao.AbstractPersistable;
import cz.cvut.fel.foodtracker.definitions.BackendConstants;

/**
 * Created by endyw on 5/16/2018.
 */

@Entity(name = BackendConstants.FRIDGE_ENTITY)
public class Fridge extends AbstractPersistable {
    private static final long serialVersionUID = 1L;

    @Column(name = BackendConstants.FRIDGE_NAME_COLUMN)
    String fridgeName;

    public Fridge() {
        this("");
    }

    public Fridge(String fridgeName) {
        this.fridgeName = fridgeName;
    }

    public String getFridgeName() {
        return fridgeName;
    }
}
