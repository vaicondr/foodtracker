package cz.cvut.fel.foodtracker;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.Intent;

import javax.inject.Inject;

import cz.cvut.fel.foodtracker.modules.DaggerApplicationContextComponent;
import cz.cvut.fel.foodtracker.services.NotificationService;
import cz.cvut.fel.foodtracker.services.NotificationTimeAlarm;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.HasServiceInjector;

/**
 * Created by vaicondr on 2/18/2018.
 */
public class FoodTrackerApplication extends Application implements HasActivityInjector,HasServiceInjector {
    @Inject
    DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;

    @Inject
    DispatchingAndroidInjector<Service> dispatchingAndroidInjectorService;


    @Override
    public void onCreate() {
        super.onCreate();
        DaggerApplicationContextComponent.builder().create(this).inject(this);

    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return dispatchingAndroidInjector;
    }

    @Override
    public AndroidInjector<Service> serviceInjector() {
        return dispatchingAndroidInjectorService;
    }
}