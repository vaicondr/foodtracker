package cz.cvut.fel.foodtracker.entities;

/**
 * Created by endyw on 5/20/2018.
 */

import javax.persistence.Column;
import javax.persistence.Entity;

import cz.cvut.fel.foodtracker.dao.AbstractPersistable;
import cz.cvut.fel.foodtracker.definitions.BackendConstants;

@Entity(name = BackendConstants.SHOPPING_LIST_ITEM_ENTITY)
public class ShoppingListItem extends AbstractPersistable {

    @Column(name = BackendConstants.PRODUCT_TYPE_ID_COLUMN)
    long productTypeId;

    @Column(name = BackendConstants.SHOPPING_ITEM_SAVED_COLUMN)
    boolean productSaved;

    @Column(name = BackendConstants.SHOPPING_LIST_QUANTITY_COLUMN)
    int quantity;

    @Column(name = BackendConstants.SHOPPING_LIST_ID_COLUMN)
    long shoppingListId;

    public ShoppingListItem(long productTypeId,boolean productSaved,int quantity,long shoppingListId) {
        this.productTypeId = productTypeId;
        this.productSaved = productSaved;
        this.quantity = quantity;
        this.shoppingListId = shoppingListId;
    }

    public ShoppingListItem() {
        this(0,false,0,0);
    }

    public long getProductTypeId() {
        return productTypeId;
    }

    public boolean isProductSaved() {
        return productSaved;
    }

    public void setProductSaved(boolean productSaved) {
        this.productSaved = productSaved;
    }

    public int getQuantity() {
        return quantity;
    }
}
