package cz.cvut.fel.foodtracker.definitions;

/**
 * Created by vaicondr on 4/4/2018.
 */

/**
 * Class used for intern representation of joined database entities
 */
public class ProductView {
    private final String categoryName;
    private final String typeName;
    private final int quantity;
    private final long expirationDate;
    private final long productUnitId;
    private final String locationStored;
    private final String units;

    public ProductView(long expirationDate, String categoryName, String typeName,int quantity,
                       long productUnitId,String locationStored,String units) {
        this.expirationDate = expirationDate;
        this.categoryName = categoryName;
        this.typeName = typeName;
        this.quantity = quantity;
        this.productUnitId = productUnitId;
        this.locationStored = locationStored;
        this.units = units;
    }

    public int getQuantity() {
        return quantity;
    }

    public long getExpirationDate() {
        return expirationDate;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getTypeName() {
        return typeName;
    }

    public long getProductUnitId() {
        return productUnitId;
    }

    public String getLocationStored() {
        return locationStored;
    }

    public String getUnits() {
        return units;
    }
}
