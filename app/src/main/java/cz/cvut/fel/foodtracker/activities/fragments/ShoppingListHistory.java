package cz.cvut.fel.foodtracker.activities.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import cz.cvut.fel.foodtracker.R;
import cz.cvut.fel.foodtracker.activities.AddFoodActivity;
import cz.cvut.fel.foodtracker.activities.MainActivity;
import cz.cvut.fel.foodtracker.activities.dialogs.ShoppingListHistoryDetailDialog;
import cz.cvut.fel.foodtracker.adapters.ShoppingListHistoryAdapter;
import cz.cvut.fel.foodtracker.controllers.Refreshable;
import cz.cvut.fel.foodtracker.definitions.BackendConstants;
import cz.cvut.fel.foodtracker.definitions.ShoppingListView;
import cz.cvut.fel.foodtracker.entities.ProductType;
import cz.cvut.fel.foodtracker.entities.ProductUnit;
import cz.cvut.fel.foodtracker.entities.ShoppingList;
import cz.cvut.fel.foodtracker.entities.ShoppingListItem;
import cz.cvut.fel.foodtracker.services.ifaces.ProductServiceInterface;
import cz.cvut.fel.foodtracker.services.ifaces.ShoppingListServiceInterface;
import dagger.android.support.AndroidSupportInjection;
import dagger.android.support.HasSupportFragmentInjector;

/**
 * Created by vaicondr on 3/3/2018.
 */

public class ShoppingListHistory extends Fragment implements Refreshable{

    @Inject
    ShoppingListServiceInterface shoppingListService;

    @Inject
    ProductServiceInterface productService;

    @BindView(R.id.shopping_history_list)
    ListView shoppingListHistoryView;

    @Override
    public void onAttach(Activity context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.history_lists_tab, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view,savedInstanceState);
        shoppingListHistoryView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        createShoppingListHistory();
    }

    @OnItemClick(R.id.shopping_history_list)
    void createListDetail(int position) {
        Intent intent = new Intent(getActivity(), ShoppingListDetailActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(BackendConstants.POSITION,position);
        startActivity(intent);
    }

    public void createShoppingListHistory() {
        List<ShoppingList> shoppingLists = shoppingListService.findAllShoppingLists();
        List<ShoppingListView> shoppingListHistory = new LinkedList<>();
        for(ShoppingList shoppingList : shoppingLists) {

            List<ShoppingListItem> items = shoppingListService
                    .findItemsByListId(shoppingList.getId());

            StringBuilder shoppingListContentBuilder = new StringBuilder();
            for(ShoppingListItem shoppingListItem : items) {
                ProductType productType = productService.findType(shoppingListItem.getProductTypeId());
                shoppingListContentBuilder.append(productType.getProductName() + " ");
            }
            ShoppingListView shoppingListView = new ShoppingListView(
                    new Date(shoppingList.getDateCreated())
                            .toString(),
                    shoppingListContentBuilder.toString());

            shoppingListHistory.add(shoppingListView);

        }
        ShoppingListHistoryAdapter shoppingListHistoryAdapter = new ShoppingListHistoryAdapter(getActivity(),shoppingListHistory);
        shoppingListHistoryView.setAdapter(shoppingListHistoryAdapter);

    }


    @Override
    public void refreshListAdapter() {
        createShoppingListHistory();
    }
}
