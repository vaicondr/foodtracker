package cz.cvut.fel.foodtracker.services;

import com.j256.ormlite.logger.Logger;
import com.j256.ormlite.logger.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import cz.cvut.fel.foodtracker.dao.FridgeDao;
import cz.cvut.fel.foodtracker.dao.ProductBrandedDao;
import cz.cvut.fel.foodtracker.dao.ProductCategoryDao;
import cz.cvut.fel.foodtracker.dao.ProductTypeDao;
import cz.cvut.fel.foodtracker.dao.ProductUnitDao;
import cz.cvut.fel.foodtracker.definitions.LoggingContexts;
import cz.cvut.fel.foodtracker.entities.Fridge;
import cz.cvut.fel.foodtracker.entities.ProductBranded;
import cz.cvut.fel.foodtracker.entities.ProductCategory;
import cz.cvut.fel.foodtracker.entities.ProductType;
import cz.cvut.fel.foodtracker.entities.ProductUnit;
import cz.cvut.fel.foodtracker.services.ifaces.ProductServiceInterface;


public class ProductService implements ProductServiceInterface {

    Logger logger = LoggerFactory.getLogger(LoggingContexts.PRODUCT_SERVICE);


    @Inject
    ProductBrandedDao productBrandedDao;

    @Inject
    ProductCategoryDao productCategoryDao;

    @Inject
    ProductTypeDao productTypeDao;

    @Inject
    ProductUnitDao productUnitDao;

    @Inject
    FridgeDao fridgeDao;

    @Inject
    public ProductService() {
    }

    public ProductService(ProductBrandedDao productBrandedDao, ProductCategoryDao productCategoryDao, ProductTypeDao productTypeDao, ProductUnitDao productUnitDao) {
        this.productBrandedDao = productBrandedDao;
        this.productCategoryDao = productCategoryDao;
        this.productTypeDao = productTypeDao;
        this.productUnitDao = productUnitDao;
    }

    @Override
    public void updateBranded(ProductBranded toUpdate) {
        productBrandedDao.update(toUpdate);
    }

    @Override
    public void updateCategoryLimit(ProductCategory productCategory,int newLimit) {
        productCategory.setNotificationTime(newLimit);
        productCategoryDao.update(productCategory);
    }

    @Override
    public void updateGlobalLimit(int newLimit) {
        List<ProductCategory> productCategories = productCategoryDao.findAll();
        for(ProductCategory productCategory : productCategories) {
            productCategory.setNotificationTime(newLimit);
            productCategoryDao.update(productCategory);
        }
    }

    //Delete and save
    @Override
    public void saveBranded(ProductBranded productBranded) {
        productBrandedDao.save(productBranded);
    }

    @Override
    public void saveType(ProductType productType) {
        productTypeDao.save(productType);
    }

    @Override
    public void saveCategory(ProductCategory productCategory) {
        productCategoryDao.save(productCategory);
    }

    @Override
    public void saveProductUnit(ProductUnit productUnit) {
        productUnitDao.save(productUnit);
    }

    @Override
    public void deleteProductUnit(ProductUnit productUnit) {
        productUnitDao.delete(productUnit);
    }

    @Override
    public void deleteProductBranded(ProductBranded productBranded) {
        productBrandedDao.delete(productBranded);
    }

    private <T> T checkThatColumnIsUniqueAndFindFirst(List<T> foundMatches) {
        if(foundMatches.isEmpty()) {
            return null;
        } else {
            if(foundMatches.size() > 1) {
                logger.error("The name columns in " + foundMatches.get(0).getClass().toString() + " should be unique!");
            }
            return foundMatches.get(0);
        }
    }

    // find one by column

    @Override
    public ProductBranded findBrandedProductByBarcode(String barcode) {
        List<ProductBranded> foundMatches = productBrandedDao.findByBarcode(barcode);
        return checkThatColumnIsUniqueAndFindFirst(foundMatches);
    }

    @Override
    public ProductCategory findCategoryByName(String name) {
        List<ProductCategory> foundMatches = productCategoryDao.findByProductCategoryName(name);
        return checkThatColumnIsUniqueAndFindFirst(foundMatches);
    }

    @Override
    public ProductType findTypeByName(String name) {
        List<ProductType> foundMatches = productTypeDao.findByProductTypeName(name);
        return checkThatColumnIsUniqueAndFindFirst(foundMatches);
    }

    @Override
    public ProductUnit findUnitByTypeIdExpirationDateAndStorageId(long expirationDate, long typeId, long storageID) {
        List<ProductUnit> foundMatches = productUnitDao.findByExpirationDateTypeIdAndStorageName(expirationDate,typeId,storageID);
        return checkThatColumnIsUniqueAndFindFirst(foundMatches);
    }



    @Override
    public void updateProductUnit(ProductUnit productUnit) {
        productUnitDao.update(productUnit);
    }

    // find One by id

    @Override
    public ProductCategory findCategory(long id) {
        return productCategoryDao.findById(id);
    }

    @Override
    public ProductType findType(long id) {
        return productTypeDao.findById(id);
    }

    @Override
    public ProductBranded findBranded(long id) {
        return productBrandedDao.findById(id);
    }

    @Override
    public ProductUnit findUnit(long id) {
        return productUnitDao.findById(id);
    }

    @Override
    public Fridge findFridge(long id) {
        return fridgeDao.findById(id);
    }


    //find List

    @Override
    public List<ProductUnit> findAllProductUnits() {
        return productUnitDao.findAll();
    }

    @Override
    public List<ProductType> findAllProductTypes() {
        return productTypeDao.findAll();
    }

    @Override
    public List<ProductCategory> findAllProductCategories() {
        return productCategoryDao.findAll();
    }

    @Override
    public Fridge findLargestFridge() {
        List<Fridge> fridges = fridgeDao.findAll();
        if(fridges.isEmpty()) {
            return null;
        }

        List<ProductUnit> productUnits = productUnitDao.findAll();
        HashMap<Long,Integer> idAppearanceCounter = new HashMap<>();
        for(Fridge fridge: fridges) {
            idAppearanceCounter.put(fridge.getId(),0);
        }

        for(ProductUnit productUnit : productUnits) {
            if(productUnit.getFridgeId() != 0) {
                int oldAmount = idAppearanceCounter.get(productUnit.getFridgeId());
                idAppearanceCounter.put(productUnit.getFridgeId(),oldAmount+1);
            }
        }

        int max = 0;
        long maxId = 0;
        for(Map.Entry<Long,Integer> mapEntry : idAppearanceCounter.entrySet()) {

            if(mapEntry.getValue() > max) {
                max = mapEntry.getValue();
                maxId = mapEntry.getKey();
            }
        }
        return fridgeDao.findById(maxId);
    }

    @Override
    public Fridge findFridgeByName(String fridgeName) {
        List<Fridge> found = fridgeDao.findByFridgeName(fridgeName);
        if(found.isEmpty()) {
            return null;
        } else {
            return found.get(0);
        }
    }

    @Override
    public void saveFridge(String fridgeName) {
        fridgeDao.save(new Fridge(fridgeName));
    }

    @Override
    public List<Fridge> findAllFridges() {
        return fridgeDao.findAll();
    }

    @Override
    public void updateProductType(ProductType productType) {
        productTypeDao.update(productType);
    }

    @Override
    public void deleteProductType(ProductType productType) {
        productTypeDao.delete(productType);
    }

    @Override
    public List<ProductUnit> findProductsByFridge(long fridgeId) {
        return productUnitDao.findProductUnitsByFridgeId(fridgeId);
    }

    @Override
    public List<ProductUnit> findProductsWithoutFridge() {
        return productUnitDao.findProductUnitsByFridgeId(0);
    }
}
