package cz.cvut.fel.foodtracker.dao;

import com.j256.ormlite.support.ConnectionSource;

import java.util.List;

import javax.inject.Inject;

import cz.cvut.fel.foodtracker.definitions.BackendConstants;
import cz.cvut.fel.foodtracker.entities.ShoppingListItem;
import cz.cvut.fel.foodtracker.helpers.DbHelper;

/**
 * Created by endyw on 5/20/2018.
 */

public class ShoppingListItemDao extends AbstractDAO<ShoppingListItem>{

    @Inject
    public ShoppingListItemDao(DbHelper dbHelper) {
        super(dbHelper.getConnectionSource());


    }
    public List<ShoppingListItem> findShoppingListItemsByListId(long shoppingListId) {
        return this.queryForEq(BackendConstants.SHOPPING_LIST_ID_COLUMN,shoppingListId);
    }
}
