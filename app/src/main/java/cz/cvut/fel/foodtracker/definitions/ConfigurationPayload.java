package cz.cvut.fel.foodtracker.definitions;

/**
 * Created by endyw on 5/4/2018.
 */

public class ConfigurationPayload {
    public static final int notificationsAllowed = 1;
    public static final int notificationsNotAllowed = 0;

}
