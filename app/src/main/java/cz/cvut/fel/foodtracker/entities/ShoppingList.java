package cz.cvut.fel.foodtracker.entities;

import javax.persistence.Column;
import javax.persistence.Entity;

import cz.cvut.fel.foodtracker.dao.AbstractPersistable;
import cz.cvut.fel.foodtracker.definitions.BackendConstants;

/**
 * Created by vaicondr on 3/18/2018.
 */

@Entity(name = BackendConstants.SHOPPING_LIST_ENTITY)
public class ShoppingList extends AbstractPersistable {

    @Column(name = BackendConstants.SHOPPING_DATE_CREATED_COLUMN,nullable = false)
    long dateCreated;

    public ShoppingList() {
        this(0);
    }

    public ShoppingList(long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public long getDateCreated() {
        return dateCreated;
    }

}
