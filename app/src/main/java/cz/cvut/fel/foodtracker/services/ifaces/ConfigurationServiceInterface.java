package cz.cvut.fel.foodtracker.services.ifaces;


import cz.cvut.fel.foodtracker.entities.Configuration;

/**
 * Created by endyw on 5/4/2018.
 */

public interface ConfigurationServiceInterface {
    void saveConfiguration(String configurationType, int configurationPayload);

    Configuration findConfiguration(String configurationType);
}
