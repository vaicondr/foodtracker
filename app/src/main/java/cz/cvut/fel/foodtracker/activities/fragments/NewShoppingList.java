package cz.cvut.fel.foodtracker.activities.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.cvut.fel.foodtracker.R;
import cz.cvut.fel.foodtracker.adapters.CurrentListAdapter;
import cz.cvut.fel.foodtracker.adapters.PopularProductsAdapter;
import cz.cvut.fel.foodtracker.controllers.ListItemsObserver;
import cz.cvut.fel.foodtracker.definitions.BackendConstants;
import cz.cvut.fel.foodtracker.entities.ProductCategory;
import cz.cvut.fel.foodtracker.entities.ProductType;
import cz.cvut.fel.foodtracker.entities.ProductUnit;
import cz.cvut.fel.foodtracker.entities.ShoppingListItem;
import cz.cvut.fel.foodtracker.helpers.ParsingHelper;
import cz.cvut.fel.foodtracker.services.ifaces.ProductServiceInterface;
import cz.cvut.fel.foodtracker.services.ifaces.ShoppingListServiceInterface;
import dagger.android.support.AndroidSupportInjection;

/**
 * Created by vaicondr on 3/3/2018.
 */

public class NewShoppingList extends Fragment {

    ListItemsObserver shoppingListSavedObserver;

    @Inject
    ShoppingListServiceInterface shoppingListService;

    @Inject
    ProductServiceInterface productService;

    Map<String,Integer> currentList;

    @BindView(R.id.raw_list_input)
    AutoCompleteTextView productSearch;

    @BindView(R.id.list_preview)
    ExpandableListView listPreview;

    @BindView(R.id.popularProducts)
    ExpandableListView expListView;

    @BindView(R.id.add_to_list_button)
    Button addToListButton;

    @BindView(R.id.save_list_button)
    Button saveListButton;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.new_list_tab, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onAttach(Activity context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view,savedInstanceState);
        currentList = new HashMap<>();
        addAutocompleteAdapter();

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                String productName = (String)expListView.getExpandableListAdapter().getChild(groupPosition,childPosition);
                if(!duplicityFoundInList(productName)) {
                    addToCurrentListView(productName);

                }
                return false;
            }
        });

        listPreview.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                Map.Entry<String,Integer> product = (Map.Entry<String,Integer>)listPreview.getExpandableListAdapter().getChild(groupPosition,childPosition);
                removeFromCurrentListView(product.getKey());
                return false;
            }

        });


        List<String> dataHeaders = createCategoryHeaders();
        HashMap<String,List<String>> items = createListContent();


        ExpandableListAdapter listAdapter = new PopularProductsAdapter(getActivity(), dataHeaders, items);
        expListView.setAdapter(listAdapter);
        initiateListPreview();
    }

    @OnClick(R.id.save_list_button)
    void saveList() {
        if(currentList.isEmpty()) {
            Toast.makeText(getActivity(), R.string.empty_list_warning,
                    Toast.LENGTH_LONG).show();
        } else {
            shoppingListService.saveShoppingList(currentList);
            currentList.clear();
            Toast.makeText(getActivity(), R.string.list_saved,
                    Toast.LENGTH_LONG).show();
            listPreview.collapseGroup(0);
            shoppingListSavedObserver.onRefreshableListChanged();
        }
    }

    @OnClick(R.id.add_to_list_button)
    void addToList() {
        String productName = productSearch.getText().toString();
        if(productName.isEmpty()) {
            Toast.makeText(getActivity(), R.string.product_name_empty,
                    Toast.LENGTH_LONG).show();

        }else if(!duplicityFoundInList(productName)) {
            addToCurrentListView(productName);
        }
        productSearch.setText(BackendConstants.EMPTY_TEXT_VIEW);
    }


    private void initiateListPreview() {
        CurrentListAdapter listAdapter = new CurrentListAdapter(getActivity(),BackendConstants.LIST_PREVIEW,currentList);
        listPreview.setAdapter(listAdapter);
    }

    private void addAutocompleteAdapter() {
        List<String> categoryNames = new LinkedList<>();

        for(ProductType productType : productService.findAllProductTypes()) {
            categoryNames.add(productType.getProductName());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_dropdown_item_1line,categoryNames.toArray(new String[categoryNames.size()]));
        productSearch.setAdapter(adapter);
    }

    private boolean duplicityFoundInList(String productName) {
        if(currentList.get(productName) != null) {
            Toast.makeText(getActivity(), R.string.duplicity_found,
                    Toast.LENGTH_LONG).show();
            return true;
        }
        return false;
    }

    private void addToCurrentListView(String productNameToAdd) {
        promptForQuantity(productNameToAdd);

    }

    private void promptForQuantity(final String productNameToAdd) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Enter the amount you desire to buy");

        final EditText input = new EditText(getActivity());
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        builder.setView(input);
        input.setText("1");
        input.selectAll();

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!ParsingHelper.isStringNumber(input.getText().toString())) {
                    Toast.makeText(getActivity(), R.string.quantity_wrong_input,
                            Toast.LENGTH_LONG).show();
                } else {
                    int quantity = Integer.parseInt(input.getText().toString());
                    finishAddingUp(productNameToAdd, quantity);
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void finishAddingUp(String productNameToAdd,int quantity) {
        currentList.put(productNameToAdd,quantity);

        CurrentListAdapter listAdapter = new CurrentListAdapter(getActivity(),
                BackendConstants.LIST_PREVIEW,currentList);
        boolean shouldExpand = false;

        if(listPreview.isGroupExpanded(0)){
            shouldExpand = true;
        }
        listPreview.setAdapter(listAdapter);

        if(shouldExpand) {
            listPreview.expandGroup(0);
        }
    }

    private void removeFromCurrentListView(String productNameToDelete) {
        currentList.remove(productNameToDelete);

        CurrentListAdapter listAdapter = new CurrentListAdapter(getActivity(),
                BackendConstants.LIST_PREVIEW,currentList);
        boolean shouldExpand = false;
        if(listPreview.isGroupExpanded(0)){
            shouldExpand = true;
        }
        listPreview.setAdapter(listAdapter);

        if(shouldExpand) {
            listPreview.expandGroup(0);
        }
    }

    private List<String> createCategoryHeaders() {
        List<ProductCategory> categoryHeaders = productService.findAllProductCategories();

        List<String> dataHeaders = new LinkedList<>();
        dataHeaders.add(BackendConstants.LIST_POPULAR);
        for(ProductCategory category : categoryHeaders) {
            dataHeaders.add(category.getCategoryName());
        }
        return dataHeaders;
    }

    private HashMap<String,List<String>> createListContent() {
        HashMap<String,List<String>> items = new HashMap<>();
        List<String> popularItems = createPopularList();
        items.put(BackendConstants.LIST_POPULAR,popularItems);

        TreeMap<String,List<String>> listContent = getCategorizedProductTypes();
        items.putAll(listContent);
        return items;
    }

    private TreeMap<String,List<String>> getCategorizedProductTypes() {
        TreeMap<String,List<String>> categorized = new TreeMap<>();
        List<ProductType> productTypes = productService.findAllProductTypes();
        for(ProductType productType : productTypes) {
            String productTypeName = productType.getProductName();
            ProductCategory productCategory = productService
                    .findCategory(productType.getProductCategoryId());
            if(productCategory == null) {
                continue;
            }
            String productCategoryName = productCategory.getCategoryName();
            if(!categorized.containsKey(productCategoryName)) {
                List<String> categorizedNames = new LinkedList<>();
                categorizedNames.add(productTypeName);
                categorized.put(productCategoryName,categorizedNames);
            } else {
                List<String> categorizedTypes = categorized.get(productCategoryName);
                categorizedTypes.add(productTypeName);
                categorized.remove(productCategoryName);
                categorized.put(productCategoryName,categorizedTypes);
            }
        }

        return categorized;
    }

    private List<String> createPopularList() {
        HashMap<String,Integer> popularity = new HashMap<>();
        List<ShoppingListItem> shoppingListItems = shoppingListService.findAllShoppingListItems();
        for(ShoppingListItem shoppingListItem : shoppingListItems) {
            ProductType productType = productService.findType(shoppingListItem.getProductTypeId());
            String productName = productType.getProductName();
            if(popularity.containsKey(productName)) {
                int quantity = popularity.get(productName);
                popularity.remove(productName);
                popularity.put(productName,++quantity);
            } else {
                popularity.put(productName,1);
            }
        }
        return getMostPopular(10,popularity);
    }

    private List<String> getMostPopular(int amountOfPopularProducts, HashMap<String,Integer> popularity) {
        List<Map.Entry<String, Integer>> popularList = new LinkedList<>(popularity.entrySet());
        Collections.sort(popularList, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });
        List<String> productNames = new LinkedList<>();

        if(popularList.size() < amountOfPopularProducts) {
            for(Map.Entry<String,Integer> mapEntry : popularList) {
                productNames.add(mapEntry.getKey());
            }
        } else {
            for (int i = 0; i > amountOfPopularProducts; i++) {
                productNames.add(popularList.get(i).getKey());
            }
        }
        return productNames;

    }

    public void setShoppingListSavedObserver(ListItemsObserver shoppingListSavedObserver) {
        this.shoppingListSavedObserver = shoppingListSavedObserver;
    }
}
