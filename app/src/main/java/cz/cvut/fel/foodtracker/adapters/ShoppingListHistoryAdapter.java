package cz.cvut.fel.foodtracker.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.j256.ormlite.logger.Logger;
import com.j256.ormlite.logger.LoggerFactory;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.cvut.fel.foodtracker.R;
import cz.cvut.fel.foodtracker.definitions.LoggingContexts;
import cz.cvut.fel.foodtracker.definitions.ShoppingListView;
import cz.cvut.fel.foodtracker.helpers.DateStringParser;

/**
 * Created by vaicondr on 4/16/2018.
 */

public class ShoppingListHistoryAdapter extends BaseAdapter{
    private List<ShoppingListView> shoppingListHistory;
    private Context context;
    Logger logger = LoggerFactory.getLogger(LoggingContexts.ADAPTERS);

    public ShoppingListHistoryAdapter(Context context, List<ShoppingListView> shoppingListHistory) {
        this.context = context;
        this.shoppingListHistory = shoppingListHistory;

        //Sorts the list by newest
        Collections.reverse(shoppingListHistory);
    }


    static class ViewHolder {
        @BindView(R.id.listCreationDate) TextView textViewDate;
        @BindView(R.id.listShoppingListContent) TextView textViewListContent;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater = LayoutInflater.from(context);

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.shopping_list_row, null);
            holder = new ViewHolder(convertView);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ShoppingListView shoppingListView = shoppingListHistory.get(position);

        if (shoppingListView != null) {
            try {

                String shoppingListText;
                if(shoppingListView.getShoppingListContent().isEmpty()) {
                    shoppingListText = "Shopping list is empty";
                } else {
                    shoppingListText = shoppingListView.getShoppingListContent();
                }
                holder.textViewListContent.setText(shoppingListText);
                holder.textViewDate.setText(DateStringParser.getOnlyDateAndYear(shoppingListView.getDateCreated()));

            } catch (Exception e) {
                logger.warn("Failed to update text views in shopping list history adapter");
            }
        }
        return convertView;
    }

    @Override
    public int getCount() {
        return shoppingListHistory.size();
    }

    @Override
    public Object getItem(int position) {
        return shoppingListHistory.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}
