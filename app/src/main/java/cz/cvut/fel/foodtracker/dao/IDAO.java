package cz.cvut.fel.foodtracker.dao;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author balikm1
 * @version 0.3
 * @since 0.1
 */
public interface IDAO<T extends IPersistable<PK>, PK extends Serializable> {
    public T findById(PK id);
    public List<T> findAll();
    public T update(T t);
    public void delete(T objectToDelete);
}
