package cz.cvut.fel.foodtracker.definitions;

/**
 * Created by endyw on 5/4/2018.
 */

public class CategorySettingsView {
    private final String categoryName;
    private  int notificationTimeInDays;

    public CategorySettingsView(String categoryName, int notificationTimeInDays) {
        this.categoryName = categoryName;
        this.notificationTimeInDays = notificationTimeInDays;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public int getNotificationTimeInDays() {
        return notificationTimeInDays;
    }

    public void setNotificationTimeInDays(int notificationTimeInDays) {
        this.notificationTimeInDays = notificationTimeInDays;
    }
}
