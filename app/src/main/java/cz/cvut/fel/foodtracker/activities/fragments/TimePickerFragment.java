package cz.cvut.fel.foodtracker.activities.fragments;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

import javax.inject.Inject;

import cz.cvut.fel.foodtracker.definitions.ConfigurationType;
import cz.cvut.fel.foodtracker.services.NotificationTimeAlarm;
import cz.cvut.fel.foodtracker.services.ifaces.ConfigurationServiceInterface;
import dagger.android.support.AndroidSupportInjection;
import cz.cvut.fel.foodtracker.R;

/**
 * Created by endyw on 5/4/2018.
 */

public class TimePickerFragment extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener {

    @Inject
    NotificationTimeAlarm notificationTimeAlarm;

    @Inject
    ConfigurationServiceInterface configurationService;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);

        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        return new TimePickerDialog(getActivity(), this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        configurationService.saveConfiguration(ConfigurationType.HOUR_OF_NOTIFICATION, hourOfDay);
        configurationService.saveConfiguration(ConfigurationType.MINUTE_OF_NOTIFICATION, minute);

        notificationTimeAlarm.setRepeatingAlarm(getActivity(),hourOfDay,minute);


        Toast.makeText(getActivity(), this.getResources().getString(R.string.hour_format,hourOfDay,minute),
                Toast.LENGTH_LONG).show();
    }
}

