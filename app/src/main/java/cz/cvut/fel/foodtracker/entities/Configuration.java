package cz.cvut.fel.foodtracker.entities;

import javax.persistence.Column;
import javax.persistence.Entity;

import cz.cvut.fel.foodtracker.dao.AbstractPersistable;
import cz.cvut.fel.foodtracker.definitions.BackendConstants;

/**
 * Created by vaicondr on 5/3/2018.
 */

@Entity(name = BackendConstants.CONFIGURATION_ENTITY)
public class Configuration  extends AbstractPersistable{
    private static final long serialVersionUID = 1L;

    @Column(name = BackendConstants.CONFIGURATION_TYPE_COLUMN)
    private String configurationType;

    @Column(name = BackendConstants.CONFIGURATION_PAYLOAD_COLUMN)
    private int configurationPayload;

    public Configuration() {
        this("",0);
    }

    public Configuration(String configurationType, int configurationPayload) {
        this.configurationType = configurationType;
        this.configurationPayload = configurationPayload;
    }

    public String getConfigurationType() {
        return configurationType;
    }

    public int getConfigurationPayload() {
        return configurationPayload;
    }

    public void setConfigurationPayload(int configurationPayload) {
        this.configurationPayload = configurationPayload;
    }
}
