package cz.cvut.fel.foodtracker.adapters;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.j256.ormlite.logger.Logger;
import com.j256.ormlite.logger.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.cvut.fel.foodtracker.R;
import cz.cvut.fel.foodtracker.activities.AddFoodActivity;
import cz.cvut.fel.foodtracker.activities.MainActivity;
import cz.cvut.fel.foodtracker.activities.fragments.ShoppingListDetailActivity;
import cz.cvut.fel.foodtracker.definitions.LoggingContexts;
import cz.cvut.fel.foodtracker.definitions.ProductView;
import cz.cvut.fel.foodtracker.definitions.ShoppingListDetailData;
import cz.cvut.fel.foodtracker.entities.ShoppingList;
import cz.cvut.fel.foodtracker.helpers.DateStringParser;

/**
 * Created by endyw on 5/19/2018.
 */

public class ListDetailAdapter extends BaseAdapter {

    ShoppingListDetailActivity shoppingListDetailActivity;

    private List<ShoppingListDetailData> detailList;
    private Logger logger = LoggerFactory.getLogger(LoggingContexts.ADAPTERS);


    public ListDetailAdapter(List<ShoppingListDetailData> detailList, ShoppingListDetailActivity shoppingListDetailActivity) {
        this.detailList = detailList;
        this.shoppingListDetailActivity = shoppingListDetailActivity;
    }

    static class ViewHolder {
        @BindView(R.id.list_detail_name) TextView detailName;
        @BindView(R.id.checklist_button) ImageButton imageButton;
        @BindView(R.id.delete_button) ImageButton deletenButton;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

    }

    @Override
    public int getCount() {
        return detailList.size();
    }

    @Override
    public Object getItem(int position) {
        return detailList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater = LayoutInflater.from(shoppingListDetailActivity);

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.list_detail_row, null);
            holder = new ViewHolder(convertView);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final ShoppingListDetailData shoppingListDetail = detailList.get(position);

        int checkedResource = R.drawable.baseline_check_circle_outline_black_18dp;
        int addResource = R.drawable.baseline_add_circle_outline_black_18dp;

        int resourceId;
        if(shoppingListDetail.isProductSaved()) {
            resourceId = checkedResource;
        } else {
            resourceId = addResource;
        }


        if (shoppingListDetail != null) {
            try {

                String text = shoppingListDetail.getProductName() + " , amount : " +
                        shoppingListDetail.getProductQuantity() + shoppingListDetail.getUnits();
                holder.detailName.setText(text);
                holder.imageButton.setImageResource(resourceId);
                if(!shoppingListDetail.isProductSaved()) {
                    holder.imageButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                          shoppingListDetailActivity.addSelectedProduct(shoppingListDetail.getProductName(),
                                  shoppingListDetail.getProductQuantity());
                        }
                    });
                }

                holder.deletenButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        shoppingListDetailActivity.removeSelectedProduct(shoppingListDetail.getProductName());
                    }
                });
            } catch (Exception e) {
                logger.warn("Failed to update text views");
            }
        }
        return convertView;
    }
}
