package cz.cvut.fel.foodtracker.helpers;

/**
 * Created by vaicondr on 4/14/2018.
 */

public class DateStringParser {
    private static String space = " ";
    private static String yearZero = "1970";

    public static String getOnlyDateAndYear(String date) {
        String[] fields = date.split(space);
        String weekDay = fields[0];
        String month = fields[1];
        String dayInMonth = fields[2];
        String year = fields[5];

        if(dateWasLeftBlank(year)) {
            return "";
        }

        StringBuilder builder = new StringBuilder();

        builder.append(month)
                .append(space)
                .append(dayInMonth)
                .append(space)
                .append(year);

        return builder.toString();
    }


    private static boolean dateWasLeftBlank(String year) {
        return year.equals(yearZero);
    }

}
