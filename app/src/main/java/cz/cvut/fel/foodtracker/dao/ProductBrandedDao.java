package cz.cvut.fel.foodtracker.dao;


import java.util.List;

import javax.inject.Inject;

import cz.cvut.fel.foodtracker.definitions.BackendConstants;
import cz.cvut.fel.foodtracker.entities.ProductBranded;
import cz.cvut.fel.foodtracker.helpers.DbHelper;


/**
 * Created by balikm1 on 9. 8. 2017.
 */

public class ProductBrandedDao extends AbstractDAO<ProductBranded> {
    @Inject
    public ProductBrandedDao(DbHelper dbHelper) {
        super(dbHelper.getConnectionSource());
    }

    public List<ProductBranded> findByBarcode(String barcode) {
        return this.queryForEq(BackendConstants.PRODUCT_BARCODE_COLUMN,barcode);
    }

}
