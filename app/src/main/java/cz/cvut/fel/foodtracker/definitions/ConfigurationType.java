package cz.cvut.fel.foodtracker.definitions;

/**
 * Created by endyw on 5/4/2018.
 */

public abstract class ConfigurationType {
    public static final String NOTIFICATIONS_ALLOWED = "NotificationsAllowed";
    public static final String HOUR_OF_NOTIFICATION = "HourOfNotif";
    public static final String MINUTE_OF_NOTIFICATION = "MinuteOfNotif";


}
