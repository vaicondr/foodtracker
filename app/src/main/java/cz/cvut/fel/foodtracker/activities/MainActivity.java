package cz.cvut.fel.foodtracker.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;


import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.cvut.fel.foodtracker.R;
import cz.cvut.fel.foodtracker.services.NotificationTimeAlarm;
import dagger.android.AndroidInjection;

public class MainActivity extends AppCompatActivity {

    @Inject
    NotificationTimeAlarm notificationTimeAlarm;

    @BindView(R.id.addFoodButton)
    Button addFoodButton;

    @BindView(R.id.shoppingListsButton)
    Button shoppingListButton;

    @BindView(R.id.fridgeButton)
    Button fridgeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AndroidInjection.inject(this);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.addFoodButton)
    void moveToAddingFood() {
        Intent intent = new Intent(MainActivity.this, AddFoodActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @OnClick(R.id.shoppingListsButton)
    void moveToShoppingListsMenu() {
        Intent intent = new Intent(MainActivity.this, ShoppingListMenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @OnClick(R.id.fridgeButton)
    void moveToFridgeContents() {
        Intent intent = new Intent(MainActivity.this, FridgeContentActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @OnClick(R.id.settingsButton)
    void moveToSettings() {
        Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

}





