package cz.cvut.fel.foodtracker.dao;

import java.io.Serializable;

/**
 *
 * @author balikm1
 * @since 0.1
 */
public interface IPersistable<PK extends Serializable> extends Serializable {
    public PK getId();
}
