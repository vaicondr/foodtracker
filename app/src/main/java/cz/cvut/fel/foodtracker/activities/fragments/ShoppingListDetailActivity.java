package cz.cvut.fel.foodtracker.activities.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.cvut.fel.foodtracker.R;
import cz.cvut.fel.foodtracker.activities.AddFoodActivity;
import cz.cvut.fel.foodtracker.activities.MainActivity;
import cz.cvut.fel.foodtracker.adapters.ListDetailAdapter;
import cz.cvut.fel.foodtracker.controllers.Refreshable;
import cz.cvut.fel.foodtracker.definitions.BackendConstants;
import cz.cvut.fel.foodtracker.definitions.ShoppingListDetailData;
import cz.cvut.fel.foodtracker.entities.ProductType;
import cz.cvut.fel.foodtracker.entities.ProductUnit;
import cz.cvut.fel.foodtracker.entities.ShoppingList;
import cz.cvut.fel.foodtracker.entities.ShoppingListItem;
import cz.cvut.fel.foodtracker.services.ShoppingListService;
import cz.cvut.fel.foodtracker.services.ifaces.ProductServiceInterface;
import dagger.android.AndroidInjection;

/**
 * Created by endyw on 5/19/2018.
 */

public class ShoppingListDetailActivity extends AppCompatActivity  {

    @Inject
    ProductServiceInterface productService;

    @Inject
    ShoppingListService shoppingService;

    @BindView(R.id.list_detail_items)
    ListView itemList;

    @BindView(R.id.shopping_list_detail_empty)
    TextView empty;

    int position;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shopping_list_detail);
        AndroidInjection.inject(this);
        ButterKnife.bind(this);
        position = getIntent().getIntExtra(BackendConstants.POSITION,0);
        createListDetail();
    }


    private void createListDetail() {
        List<ShoppingList> shoppingLists = shoppingService.findAllShoppingLists();
        List<ShoppingListItem> shoppingListItems = shoppingService.findItemsByListId(shoppingLists.size() - position);


        List<ShoppingListDetailData> details = new LinkedList<>();
        for(ShoppingListItem shoppingListItem : shoppingListItems) {
            ProductType productType = productService.findType(shoppingListItem.getProductTypeId());

            details.add(new ShoppingListDetailData(productType.getProductName(),
                    shoppingListItem.getQuantity(),shoppingListItem.isProductSaved(),productType.getUnits()));
        }

        if(details.isEmpty()) {
            empty.setText("Shopping list is empty");
        }

        ListDetailAdapter listDetailAdapter = new ListDetailAdapter(details,this);
        itemList.setAdapter(listDetailAdapter);
    }

    public void addSelectedProduct(String productName,int quantity) {
        Intent intent = new Intent(ShoppingListDetailActivity.this, AddFoodActivity.class);

        intent.putExtra(BackendConstants.PRODUCT_TYPE_NAME_COLUMN,productName);
        intent.putExtra(BackendConstants.PRODUCT_QUANTITY,quantity);

        startActivityForResult(intent,BackendConstants.ADD_FOOD_REQUEST_CODE);
    }

    public void checkSelectedProduct(String productName) {
        List<ShoppingList> shoppingLists = shoppingService.findAllShoppingLists();
        List<ShoppingListItem> shoppingListItems = shoppingService.findItemsByListId(shoppingLists.size() - position);
        for(ShoppingListItem shoppingListItem : shoppingListItems) {
            ProductType productType = productService.findType(shoppingListItem.getProductTypeId());
            if(productType.getProductName().equals(productName)) {
                shoppingListItem.setProductSaved(true);
                shoppingService.updateShoppingItem(shoppingListItem);
                return;
            }
        }

    }

    public void removeSelectedProduct(String productName) {
        List<ShoppingList> shoppingLists = shoppingService.findAllShoppingLists();
        List<ShoppingListItem> shoppingListItems = shoppingService.findItemsByListId(shoppingLists.size() - position);
        for(ShoppingListItem shoppingListItem : shoppingListItems) {
            ProductType productType = productService.findType(shoppingListItem.getProductTypeId());
            if(productType.getProductName().equals(productName)) {
                shoppingListItem.setProductSaved(true);
                shoppingService.deleteShoppingItem(shoppingListItem);
                createListDetail();
                return;
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode,int resultCode,Intent data) {
        if(resultCode == Activity.RESULT_OK) {
            String checked = data.getStringExtra(BackendConstants.PRODUCT_TYPE_NAME_COLUMN);
            checkSelectedProduct(checked);
            createListDetail();
        }
    }
}
