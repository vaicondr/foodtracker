package cz.cvut.fel.foodtracker.definitions;

/**
 * Created by vaicondr on 4/16/2018.
 */

public class ShoppingListView {
    private final String dateCreated;
    private final String shoppingListContent;

    public ShoppingListView(String dateCreated, String shoppingListContent) {
        this.dateCreated = dateCreated;
        this.shoppingListContent = shoppingListContent;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public String getShoppingListContent() {
        return shoppingListContent;
    }
}
