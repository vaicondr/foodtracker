package cz.cvut.fel.foodtracker.dao;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.support.ConnectionSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.ParameterizedType;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import cz.cvut.fel.foodtracker.exceptions.DataAccessException;


/**
 * Abstract data access object supporting CRUD operations for OrmLite JPA
 * entities.
 *
 * @author balikm1
 * @author plockare
 * @since 0.4
 */
public class AbstractDAO<T extends IPersistable<Long>> implements IDAO<T, Long> {

    private static Logger LOGGER = LoggerFactory.getLogger("ASF");

    private ConnectionSource connectionSource;

    //<editor-fold defaultstate="collapsed" desc="Initialization">
    public AbstractDAO(ConnectionSource connectionSource) {
        this.connectionSource = connectionSource;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Public properties">
    public ConnectionSource getConnectionSource() {
        return connectionSource;
    }

    public void setConnectionSource(ConnectionSource connectionSource) {
        this.connectionSource = connectionSource;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Protected methods">
    protected Dao<T, Long> getOrmliteDao() {
        try {
            return DaoManager.<Dao<T, Long>, T>createDao(getConnectionSource(), getPersistentClass());
        } catch (SQLException ex) {
            LOGGER.error(String.format("Error retrieving DAO object for class %s", getPersistentClass().getName()), ex);
            throw new DataAccessException(String.format("Error retrieving DAO object for class %s", getPersistentClass().getName()), ex);
        }
    }

    @SuppressWarnings("unchecked")
    protected Class<T> getPersistentClass() {
        return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public void save(T entity) {
        int rowCount = 0;
        try {
            rowCount = getOrmliteDao().create(entity);
        } catch (SQLException ex) {
            LOGGER.error(String.format("Error saving entity %s", entity), ex);
            throw new DataAccessException(String.format("Error saving entity %s", entity), ex);
        }
        if (rowCount != 1) {
            throw new DataAccessException(String.format("Error in create: row count = %d", rowCount));
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="IDAO implementation">
    @Override
    public T findById(Long id) {
        try {
            return getOrmliteDao().queryForId(id);
        } catch (SQLException ex) {
            LOGGER.error(String.format("Error finding entity with ID %ld.", id), ex);
            throw new DataAccessException(String.format("Error finding entity with ID %ld.", id), ex);
        }
    }

    public List<T> queryForEq(String s, Object o) {
        try {
            return getOrmliteDao().queryForEq(s, o);
        } catch (SQLException ex) {
            LOGGER.error(String.format("Error finding entity by column %s.", s), ex);
            throw new DataAccessException(String.format("Error finding entity by column %s.", s), ex);
        }
    }

    public T findOne(PreparedQuery<T> query) {
        try {
            return getOrmliteDao().queryForFirst(query);
        } catch (SQLException ex) {
            LOGGER.error(String.format("Error finding one."), ex);
            throw new DataAccessException(String.format("Error finding one."), ex);
        }
    }

    @Override
    public List<T> findAll() {
        try {
            return getOrmliteDao().queryForAll();
        } catch (SQLException ex) {
            LOGGER.error("Error in findAll query.", ex);
            throw new DataAccessException(ex);
        }
    }

    @Override
    public T update(T entity) {
        try {
            getOrmliteDao().update(entity);
        } catch (SQLException ex) {
            LOGGER.error(String.format("Error update entity %s.", entity), ex);
            throw new DataAccessException(String.format("Error update entity %s.", entity), ex);
        }
        return entity;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void delete(T objectToDelete) {
        try {
            getOrmliteDao().delete(objectToDelete);
        } catch (SQLException ex) {
            java.util.logging.Logger.getLogger(AbstractDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
