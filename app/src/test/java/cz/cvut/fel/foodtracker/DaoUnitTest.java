package cz.cvut.fel.foodtracker;

import com.j256.ormlite.db.DatabaseType;
import com.j256.ormlite.field.DataPersister;
import com.j256.ormlite.field.FieldConverter;
import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.support.DatabaseConnection;
import com.j256.ormlite.support.GeneratedKeyHolder;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.SQLException;

import static org.mockito.Mockito.*;

import cz.cvut.fel.foodtracker.dao.ProductBrandedDao;
import cz.cvut.fel.foodtracker.entities.ProductBranded;
import cz.cvut.fel.foodtracker.helpers.DbHelper;


@RunWith(MockitoJUnitRunner.class)
public class DaoUnitTest {
    @Mock
    DbHelper dbHelper;

    @Mock
    ConnectionSource connectionSource;

    @Mock
    DatabaseType databaseType;

    @Mock
    DatabaseConnection connection;

    @Mock
    GeneratedKeyHolder generatedKeyHolder;

    @Spy
    FieldConverter fieldConverter;

    ProductBrandedDao productBrandedDao;

    @Before
    public void setUp() throws SQLException{
        when(dbHelper.getConnectionSource()).thenReturn(connectionSource);
        when(connectionSource.getDatabaseType()).thenReturn(databaseType);
        when(connectionSource.getReadOnlyConnection()).thenReturn(connection);
        when(connectionSource.getReadWriteConnection()).thenReturn(connection);
        when(databaseType.getFieldConverter(any(DataPersister.class))).thenReturn(fieldConverter);
        when(connection.insert(anyString(),any(Object[].class),any(FieldType[].class),any(GeneratedKeyHolder.class))).thenReturn(1);


        productBrandedDao = new ProductBrandedDao(dbHelper);

    }

    @Test
    public void daoUsesDbHelperConnectionSource() throws Exception {
        verify(dbHelper,times(1)).getConnectionSource();
    }

    @Test
    public void findByIdArgumentsPassedCorrectly() throws SQLException {
        long productId = 0L;
        productBrandedDao.findById(productId);
        verify(fieldConverter).javaToSqlArg(ArgumentMatchers.any(FieldType.class),ArgumentMatchers.eq(0L));
    }


    @Test
    public void delete() throws SQLException {
        productBrandedDao.delete(new ProductBranded());
        verify(connection,times(1)).delete(anyString(),any(Object[].class),any(FieldType[].class));
    }





}