package cz.cvut.fel.foodtracker.modules;

import javax.inject.Singleton;

import cz.cvut.fel.foodtracker.FoodTrackerApplication;
import dagger.Component;
import dagger.android.AndroidInjector;

/**
 * Created by vaicondr on 2/18/2018.
 */

@Singleton
@Component(modules = ApplicationContextModule.class)
interface ApplicationContextComponent extends AndroidInjector<FoodTrackerApplication> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<FoodTrackerApplication> {
    }
}