package cz.cvut.fel.foodtracker.entities;

import javax.persistence.Column;
import javax.persistence.Entity;


import cz.cvut.fel.foodtracker.dao.AbstractPersistable;
import cz.cvut.fel.foodtracker.definitions.BackendConstants;


@Entity(name = BackendConstants.PRODUCT_BRANDED_ENTITY)
public class ProductBranded extends AbstractPersistable {

    private static final long serialVersionUID = 1L;

    @Column(name = BackendConstants.PRODUCT_BARCODE_COLUMN)
    private String productBarcode;

    @Column(name = BackendConstants.PRODUCT_TYPE_ID_COLUMN,nullable = false)
    private long productTypeId;

    public ProductBranded() {
        this(0,"");
    }

    public ProductBranded(long typeId) {
        this.productBarcode = "";
        this.productTypeId = typeId;
    }

    public ProductBranded(long typeId, String productBarcode) {
        this.productTypeId = typeId;
        this.productBarcode = productBarcode;
    }

    public long getProductTypeId() {
        return productTypeId;
    }

    public String getProductBarcode() {
        return productBarcode;
    }

    public void setProductTypeId(long productTypeId) {
        this.productTypeId = productTypeId;
    }
}