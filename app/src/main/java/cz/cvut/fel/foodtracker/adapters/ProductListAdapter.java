package cz.cvut.fel.foodtracker.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.j256.ormlite.logger.Logger;
import com.j256.ormlite.logger.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.cvut.fel.foodtracker.R;
import cz.cvut.fel.foodtracker.definitions.LoggingContexts;
import cz.cvut.fel.foodtracker.definitions.ProductView;
import cz.cvut.fel.foodtracker.entities.Fridge;
import cz.cvut.fel.foodtracker.helpers.DateStringParser;


public class ProductListAdapter extends BaseAdapter {


    private List<ProductView> productList;
    private HashMap<String,Integer> fridgeColors;
    private Context context;
    private Logger logger = LoggerFactory.getLogger(LoggingContexts.ADAPTERS);

    public ProductListAdapter(Context context, List<ProductView> products, List<Fridge> fridges) {
        this.context = context;
        this.productList = products;
        fridgeColors = new HashMap<>();
        createColourMapForFridges(fridges);
    }

    private List<Integer> createColorList() {
        LinkedList<Integer> colorList = new LinkedList<>();
        colorList.add(Color.LTGRAY);
        colorList.add(Color.YELLOW);
        colorList.add(Color.GREEN);
        colorList.add(Color.RED);
        colorList.add(Color.BLUE);
        colorList.add(Color.CYAN);
        colorList.add(Color.MAGENTA);
        colorList.add(Color.DKGRAY);
        colorList.add(Color.BLACK);

        return colorList;
    }

    private void createColourMapForFridges(List<Fridge> fridges) {
        List<Integer> colorList = createColorList();
        for (int i = 0; i < fridges.size() && i < colorList.size(); i++) {
            if(i == 0) {
            }
            fridgeColors.put(fridges.get(i).getFridgeName(),colorList.get(i));

        }
        fridgeColors.put("",Color.WHITE);

    }

    static class ViewHolder {
        @BindView(R.id.listProductName) TextView textViewProductName;
        @BindView(R.id.listCategoryName) TextView textViewProductCategoryName;
        @BindView(R.id.listProductExpirationDate) TextView textViewProductExpirationDate;
        @BindView(R.id.listProductQuantity) TextView textViewQuantity;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        LayoutInflater inflater = LayoutInflater.from(context);

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.product_row, null);
            holder = new ViewHolder(convertView);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ProductView productView = productList.get(position);


        if (productView != null) {
            try {
                holder.textViewProductName.setText(productView.getTypeName());
                holder.textViewProductCategoryName.setText(productView.getCategoryName());
                holder.textViewProductExpirationDate.setText(DateStringParser.getOnlyDateAndYear(
                        new Date(productView.getExpirationDate()).toString()));
                holder.textViewQuantity.setText(context.getResources().getString(R.string.quantity_format,
                        productView.getQuantity(),productView.getUnits()));
            } catch (Exception e) {
                logger.warn("Failed to update text views");
            }
        }

        convertView.setBackgroundColor(fridgeColors.get(productView.getLocationStored()));
        return convertView;
    }

    @Override
    public int getCount() {
        return productList.size();
    }

    @Override
    public Object getItem(int pos) {
        return productList.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }
}