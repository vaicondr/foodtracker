package cz.cvut.fel.foodtracker.dao;

import java.util.List;

import javax.inject.Inject;

import cz.cvut.fel.foodtracker.definitions.BackendConstants;
import cz.cvut.fel.foodtracker.entities.ProductUnit;
import cz.cvut.fel.foodtracker.helpers.DbHelper;

/**
 * Created by vaicondr on 4/4/2018.
 */

public class ProductUnitDao extends AbstractDAO<ProductUnit> {
    @Inject
    public ProductUnitDao(DbHelper dbHelper) {
        super(dbHelper.getConnectionSource());
    }

    public List<ProductUnit> findByExpirationDateTypeIdAndStorageName(long expirationDate, long typeId,long storage) {
        List<ProductUnit> barcodeMatches = this.queryForEq(BackendConstants.PRODUCT_TYPE_ID_COLUMN,typeId);
        List<ProductUnit> expirationMatches = this.queryForEq(BackendConstants.PRODUCT_EXPIRATION_COLUMN,expirationDate);
        List<ProductUnit> storageMatches = this.queryForEq(BackendConstants.PRODUCT_LOCATION_STORED_ID,storage);

        barcodeMatches.retainAll(expirationMatches);
        barcodeMatches.retainAll(storageMatches);
        return barcodeMatches;
    }

    public List<ProductUnit> findProductUnitsByFridgeId(long fridgeId) {
        return queryForEq(BackendConstants.PRODUCT_LOCATION_STORED_ID,fridgeId);
    }
}
