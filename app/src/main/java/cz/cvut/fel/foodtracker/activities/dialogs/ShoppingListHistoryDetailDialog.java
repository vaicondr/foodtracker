package cz.cvut.fel.foodtracker.activities.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import java.util.ArrayList;
import java.util.List;

import cz.cvut.fel.foodtracker.R;
import cz.cvut.fel.foodtracker.definitions.BackendConstants;


/**
 * Created by vaicondr on 4/21/2018.
 */

public class ShoppingListHistoryDetailDialog extends DialogFragment {
    List<Integer> mSelectedItems;

    String[] listItems;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        listItems = getArguments().getStringArray(BackendConstants.LIST_ITEMS);
        mSelectedItems = new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.shopping_list_detail)
                .setMultiChoiceItems(listItems, null,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which,
                                                boolean isChecked) {
                                if (isChecked) {
                                    mSelectedItems.add(which);
                                } else if (mSelectedItems.contains(which)) {
                                    mSelectedItems.remove(Integer.valueOf(which));
                                }
                            }
                        })
                .setPositiveButton(R.string.close, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });


        return builder.create();
    }


}
