package cz.cvut.fel.foodtracker.entities;

import javax.persistence.Column;
import javax.persistence.Entity;

import cz.cvut.fel.foodtracker.dao.AbstractPersistable;
import cz.cvut.fel.foodtracker.definitions.BackendConstants;

/**
 * Created by vaicondr on 3/18/2018.
 */

@Entity(name = BackendConstants.PRODUCT_TYPE_ENTITY)
public class ProductType extends AbstractPersistable {

    private static final long serialVersionUID = 1L;

    @Column(name = BackendConstants.PRODUCT_CATEGORY_ID_COLUMN, nullable = false)
    private long productCategoryId;

    @Column(name = BackendConstants.PRODUCT_TYPE_NAME_COLUMN, nullable = false)
    private String productName;

    @Column(name = BackendConstants.UNITS,nullable = false)
    private String units;

    public ProductType() {
        this(0,"","");
    }

    public ProductType(long categoryId, String productName,String units) {
        this.productCategoryId = categoryId;
        this.productName = productName;
        this.units = units;
    }


    public long getProductCategoryId() {
        return productCategoryId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductCategoryId(long productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}

