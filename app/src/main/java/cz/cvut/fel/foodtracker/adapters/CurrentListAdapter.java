package cz.cvut.fel.foodtracker.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cz.cvut.fel.foodtracker.R;

/**
 * Created by endyw on 5/19/2018.
 */

public class CurrentListAdapter extends BaseExpandableListAdapter {
    private Context _context;
    private String header;
    private List<Map.Entry<String,Integer>> _listDataChild;

    public CurrentListAdapter(Context _context, String header, Map<String, Integer> listDataChild) {
        this._context = _context;
        this.header = header;
        this._listDataChild = new ArrayList<>();
        _listDataChild.addAll(listDataChild.entrySet());
    }

    @Override
    public int getGroupCount() {
        return 1;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return _listDataChild.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return header;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return _listDataChild.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.popular_products_header, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.popularListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(header);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        Map.Entry<String,Integer> child = (Map.Entry<String,Integer>) getChild(groupPosition, childPosition);
        String itemName = child.getKey();
        int itemQuantity = child.getValue();


        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.current_shopping_list_item, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.current_list_item_name);
        TextView txtListQuantity = (TextView) convertView.findViewById(R.id.current_list_item_quantity);



        txtListChild.setText(itemName);
        txtListQuantity.setText(String.valueOf(itemQuantity));

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
