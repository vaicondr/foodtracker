package cz.cvut.fel.foodtracker.dao;

import java.util.List;

import javax.inject.Inject;

import cz.cvut.fel.foodtracker.definitions.BackendConstants;
import cz.cvut.fel.foodtracker.entities.ProductCategory;
import cz.cvut.fel.foodtracker.entities.ProductType;
import cz.cvut.fel.foodtracker.helpers.DbHelper;

/**
 * Created by vaicondr on 3/18/2018.
 */

public class ProductTypeDao extends AbstractDAO<ProductType> {
    @Inject
    public ProductTypeDao(DbHelper dbHelper) {
        super(dbHelper.getConnectionSource());
    }

    public List<ProductType> findByProductTypeName(String productName) {
        return this.queryForEq(BackendConstants.PRODUCT_TYPE_NAME_COLUMN,productName);
    }

}