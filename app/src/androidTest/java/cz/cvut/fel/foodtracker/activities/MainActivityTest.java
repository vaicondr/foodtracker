package cz.cvut.fel.foodtracker.activities;


import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.action.ViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import cz.cvut.fel.foodtracker.R;

import static android.support.test.espresso.Espresso.closeSoftKeyboard;
import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void cantAddFoodWithoutFillingData() {
        onView(withId(R.id.addFoodButton)).perform(click());
        onView(withId(R.id.add_button)).perform(click());

        onView(withText(R.string.required_fields_empty)).inRoot(withDecorView(not(is(mActivityTestRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));

    }

    @Test
    public void addsFood() throws InterruptedException {
        onView(withId(R.id.addFoodButton)).perform(click());
        onView(withId(R.id.product_edit)).perform(typeText("testProduct"));
        onView(withId(R.id.product_category_edit)).perform(typeText("testCategory"), ViewActions.closeSoftKeyboard());
        Thread.sleep(1000);
        onView(withId(R.id.add_button)).perform(click());


        onView(withText(R.string.product_saved)).inRoot(withDecorView(not(is(mActivityTestRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));

    }


    @Test
    public void showsEmptyFridge() {
        onView(withId(R.id.fridgeButton)).perform(click());
        onView(withId(R.id.fridge_empty_text)).check(matches(isDisplayed()));
    }

    @Test
    public void showsEmptyCategories() {
        onView(withId(R.id.settingsButton)).perform(click());
        onView(withId(R.id.category_empty)).check(matches(isDisplayed()));
    }

}
