package cz.cvut.fel.foodtracker.controllers;

import javax.inject.Inject;

import cz.cvut.fel.foodtracker.entities.ProductCategory;
import cz.cvut.fel.foodtracker.entities.ProductType;
import cz.cvut.fel.foodtracker.entities.ProductUnit;
import cz.cvut.fel.foodtracker.services.ifaces.ProductServiceInterface;

/**
 * Created by vaicondr on 4/29/2018.
 */

public class ProductSpecificationDialogListener {

    private ListItemsObserver observer;

    @Inject
    ProductServiceInterface productService;

    @Inject
    public ProductSpecificationDialogListener() {
    }

    public void onProductDelete(long productId) {
        ProductUnit productUnit = productService.findUnit(productId);
        productService.deleteProductUnit(productUnit);
        observer.onRefreshableListChanged();
    }

    public void onProductTypeUpdate(long typeId,String newCategoryName) {
        ProductCategory productCategory = productService.findCategoryByName(newCategoryName);

        if(productCategory == null) {
            productCategory = new ProductCategory(newCategoryName,1);
            productService.saveCategory(productCategory);
            productCategory = productService.findCategoryByName(newCategoryName);
        }
        ProductType productType = productService.findType(typeId);
        productType.setProductCategoryId(productCategory.getId());
        productService.updateProductType(productType);
        observer.onRefreshableListChanged();

    }

    public void onProductQuantityUpdate(long productId, int newQuantity) {
        ProductUnit productUnit = productService.findUnit(productId);
        if(newQuantity > 0) {
            productUnit.setQuantity(newQuantity);
            productService.updateProductUnit(productUnit);
        } else {
            productService.deleteProductUnit(productUnit);
        }
        observer.onRefreshableListChanged();

    }

    public void setObservedActivity(Refreshable refreshable) {
        observer = new ListItemsObserver(refreshable);
    }
}
