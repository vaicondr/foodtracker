package cz.cvut.fel.foodtracker.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.logger.Logger;
import com.j256.ormlite.logger.LoggerFactory;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import javax.inject.Inject;

import cz.cvut.fel.foodtracker.FoodTrackerApplication;
import cz.cvut.fel.foodtracker.definitions.LoggingContexts;
import cz.cvut.fel.foodtracker.entities.Configuration;
import cz.cvut.fel.foodtracker.entities.Fridge;
import cz.cvut.fel.foodtracker.entities.ProductBranded;
import cz.cvut.fel.foodtracker.entities.ProductCategory;
import cz.cvut.fel.foodtracker.entities.ProductType;
import cz.cvut.fel.foodtracker.entities.ProductUnit;
import cz.cvut.fel.foodtracker.entities.ShoppingList;
import cz.cvut.fel.foodtracker.entities.ShoppingListItem;


public class DbHelper extends OrmLiteSqliteOpenHelper {

    private final static String DATABASENAME = "Foodtracker.db";
    private final static int DATABASEVERSION = 15;

    Logger logger = LoggerFactory.getLogger(LoggingContexts.DATABASE);

    @Inject
    public DbHelper(FoodTrackerApplication context) {
        super(context, DATABASENAME, null, DATABASEVERSION);
    }


    public DbHelper(Context context) {
        super(context, DATABASENAME, null, DATABASEVERSION);
    }

    public DbHelper(Context context, String name, int version) {
        super(context,name,null,version);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            logger.info("Creating new database tables");
            TableUtils.createTableIfNotExists(connectionSource, ProductBranded.class);
            TableUtils.createTableIfNotExists(connectionSource, ProductType.class);
            TableUtils.createTableIfNotExists(connectionSource, ProductCategory.class);
            TableUtils.createTableIfNotExists(connectionSource, ProductUnit.class);
            TableUtils.createTableIfNotExists(connectionSource, ShoppingList.class);
            TableUtils.createTableIfNotExists(connectionSource, Configuration.class);
            TableUtils.createTableIfNotExists(connectionSource, Fridge.class);
            TableUtils.createTableIfNotExists(connectionSource, ShoppingListItem.class);

        } catch (Exception e) {
            logger.error("Failed to create tables");
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource,
                          int oldVersion, int newVersion) {
        try {
            logger.info(String.format("Upgrading database version from : %d to %d",oldVersion,newVersion));
            TableUtils.dropTable(connectionSource, ProductBranded.class,true);
            TableUtils.dropTable(connectionSource, ProductType.class,true);
            TableUtils.dropTable(connectionSource, ProductCategory.class,true);
            TableUtils.dropTable(connectionSource, ProductUnit.class,true);
            TableUtils.dropTable(connectionSource, ShoppingList.class,true);
            TableUtils.dropTable(connectionSource, Configuration.class,true);
            TableUtils.dropTable(connectionSource, Fridge.class,true);
            TableUtils.dropTable(connectionSource, ShoppingListItem.class,true);

            onCreate(db, connectionSource);
        } catch (SQLException e) {
            logger.error("Can't upgrade databases", e);
            throw new RuntimeException(e);
        }
    }




}
