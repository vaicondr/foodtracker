package cz.cvut.fel.foodtracker.dao;

import java.util.List;

import javax.inject.Inject;

import cz.cvut.fel.foodtracker.definitions.BackendConstants;
import cz.cvut.fel.foodtracker.entities.ProductCategory;
import cz.cvut.fel.foodtracker.helpers.DbHelper;

/**
 * Created by vaicondr on 3/18/2018.
 */

public class ProductCategoryDao extends AbstractDAO<ProductCategory> {
    @Inject
    public ProductCategoryDao(DbHelper dbHelper) {
        super(dbHelper.getConnectionSource());
    }

    public List<ProductCategory> findByProductCategoryName(String categoryName) {
        return this.queryForEq(BackendConstants.PRODUCT_CATEGORY_NAME_COLUMN,categoryName);
    }
}
