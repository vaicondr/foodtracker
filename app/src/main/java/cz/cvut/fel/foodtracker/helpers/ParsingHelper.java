package cz.cvut.fel.foodtracker.helpers;

/**
 * Created by endyw on 5/19/2018.
 */

public class ParsingHelper {
    public static boolean isStringNumber(String string) {
        return string.matches("\\d+(?:\\.\\d+)?");
    }

}
