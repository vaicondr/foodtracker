package cz.cvut.fel.foodtracker.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.cvut.fel.foodtracker.R;
import cz.cvut.fel.foodtracker.activities.dialogs.NewCategoryOrStorageDialog;
import cz.cvut.fel.foodtracker.codescanning.IntentIntegrator;
import cz.cvut.fel.foodtracker.codescanning.IntentResult;
import cz.cvut.fel.foodtracker.controllers.AddFoodController;
import cz.cvut.fel.foodtracker.controllers.ProductSavedListener;
import cz.cvut.fel.foodtracker.definitions.BackendConstants;
import cz.cvut.fel.foodtracker.definitions.UnitType;
import cz.cvut.fel.foodtracker.helpers.ParsingHelper;
import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

/**
 * Created by vaicondr on 8/18/2017.
 */

public class AddFoodActivity extends AppCompatActivity implements HasSupportFragmentInjector,ProductSavedListener{

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    @BindView(R.id.exp_date)
    TextView date;

    @BindView(R.id.scanned_code)
    TextView scannedCode;

    @BindView(R.id.quantity_edit)
    EditText quantity;

    @BindView(R.id.product_edit)
    AutoCompleteTextView productName;

    @BindView(R.id.product_category_edit)
    AutoCompleteTextView categoryType;

    @BindView(R.id.exp_button)
    Button selectDate;

    @BindView(R.id.location_edit)
    AutoCompleteTextView storageLocation;

    @Inject
    AddFoodController addFoodController;

    @BindView(R.id.productName)
    TextView productNameField;

    @BindView(R.id.productCategory)
    TextView productCategoryField;

    @BindView(R.id.quantity)
    TextView quantityField;

    @BindView(R.id.units_spinner)
    Spinner unitsSpinner;

    int defaultColor;
    long dateValue;

    boolean passedFromShoppingList = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_food_activity);
        AndroidInjection.inject(this);
        ButterKnife.bind(this);

        initiateViews(savedInstanceState);
        refreshAutocompleteAdapters();
        defaultColor = quantityField.getCurrentTextColor();
        tryToPrefillIfOpenedFromShoppingList();
    }

    private void tryToPrefillIfOpenedFromShoppingList() {
        String productNamePassed = getIntent().getStringExtra(BackendConstants.PRODUCT_TYPE_NAME_COLUMN);
        if(productNamePassed != null) {
            passedFromShoppingList = true;
            int passedQuantity = getIntent().getIntExtra(BackendConstants.PRODUCT_QUANTITY,1);
            productName.setText(productNamePassed);
            addFoodController.tryToPrefillDataBasedOnProductName(productNamePassed,categoryType,
                    unitsSpinner);
            quantity.setText(Integer.toString(passedQuantity));
        }

    }

    private void refreshAutocompleteAdapters() {
        addFoodController.addAdapterToProductCategoryView(AddFoodActivity.this,categoryType);
        addFoodController.addAdapterToProductNameView(AddFoodActivity.this,productName);
        addFoodController.addAdapterToStorage(AddFoodActivity.this,storageLocation);
    }

    private void initiateViews(Bundle savedInstanceState) {
        addFoodController.addListenerToSelectDate(AddFoodActivity.this,selectDate,date);
        addFoodController.addListenerToProductName(productName,categoryType,unitsSpinner);
        storageLocation.setText(addFoodController.findLargestStorage());
        if(savedInstanceState != null) {
            String savedCode = savedInstanceState.getString(BackendConstants.PRODUCT_BARCODE_COLUMN);
            if (!savedCode.isEmpty()) {
                scannedCode.setText(savedCode);
            }
            String savedDate = savedInstanceState.getString(BackendConstants.PRODUCT_EXPIRATION);
            if (!savedDate.isEmpty()) {
                date.setText(savedDate);
            }
        }
        createUnitSpinner();
    }

    private void createUnitSpinner() {
        unitsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        List<UnitType> units = Arrays.asList(UnitType.values());
        String[] unitNames = new String[units.size()];
        for (int i = 0; i < units.size() ; i++) {
            unitNames[i] = units.get(i).toString();
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item,
                unitNames);
        dataAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        unitsSpinner.setAdapter(dataAdapter);
    }

    @OnClick(R.id.scan_button)
    public void scanCode(View view) {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.initiateScan();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode,
                intent);
        if (scanResult != null && resultCode != 0) {
            scannedCode.setText(scanResult.getContents());
            addFoodController.tryToPrefillDataBasedOnScannedCode(scanResult.getContents(),
                    productName,categoryType,unitsSpinner);
        }


    }

    @OnClick(R.id.add_button)
    public void addProduct(View view) {
        resetHiglighted();
        if (categoryType.getText().toString().isEmpty() ||
                productName.getText().toString().isEmpty() ||
                quantity.getText().toString().isEmpty()) {
            Toast.makeText(this, R.string.required_fields_empty,
                    Toast.LENGTH_LONG).show();
            highlightWrongInput();

        } else if(!ParsingHelper.isStringNumber(quantity.getText().toString())) {
            Toast.makeText(this, R.string.quantity_wrong_input,
                    Toast.LENGTH_LONG).show();
            highlightWrongInput();

        } else {
            checkIfCategoryOrStorageExistsAndSaveProduct();
        }
    }


    private void highlightWrongInput() {
        if (categoryType.getText().toString().isEmpty()) {
            productCategoryField.setTextColor(Color.RED);
        }
        if (productName.getText().toString().isEmpty()) {
            productNameField.setTextColor(Color.RED);
        }

        String quantityString = quantity.getText().toString();
        if (quantityString.isEmpty() || !ParsingHelper.isStringNumber(quantityString) ) {
            quantityField.setTextColor(Color.RED);
        }
    }

    private void resetHiglighted() {
        productCategoryField.setTextColor(defaultColor);
        productNameField.setTextColor(defaultColor);
        quantityField.setTextColor(defaultColor);
    }

    public void onProductSaved() {
        if(passedFromShoppingList) {
            Intent resultIntent = new Intent();
            resultIntent.putExtra(BackendConstants.PRODUCT_TYPE_NAME_COLUMN,getIntent()
                    .getStringExtra(BackendConstants.PRODUCT_TYPE_NAME_COLUMN));
            setResult(Activity.RESULT_OK,resultIntent);
            finish();
        }
        Toast.makeText(this, R.string.product_saved,
                Toast.LENGTH_LONG).show();

        refreshAutocompleteAdapters();
        clearViews();
    }

    private void checkIfCategoryOrStorageExistsAndSaveProduct() {
        String category = categoryType.getText().toString();
        String storageName = storageLocation.getText().toString();


        boolean categoryExists = addFoodController.categoryExists(category);
        boolean storageExists = addFoodController.storageExists(storageName) || storageName.isEmpty();

        if(categoryExists && storageExists) {
            addFoodController.addProduct(category, productName.getText().toString(),
                    scannedCode.getText().toString(), quantity.getText().toString(),
                    storageName,unitsSpinner.getSelectedItem().toString());
            onProductSaved();
        } else {
            NewCategoryOrStorageDialog newCategoryDialog = new NewCategoryOrStorageDialog();
            Bundle args = new Bundle();
            args.putString(BackendConstants.PRODUCT_CATEGORY,category);
            args.putString(BackendConstants.LOCATION,storageName);
            args.putBoolean(BackendConstants.CATEGORY_EXISTS,categoryExists);
            args.putBoolean(BackendConstants.STORAGE_EXISTS,storageExists);
            args.putLong(BackendConstants.DATE_VALUE,addFoodController.getDateValue());
            args.putString(BackendConstants.UNITS,unitsSpinner.getSelectedItem().toString());

            args.putString(BackendConstants.PRODUCT_NAME,productName.getText().toString());
            args.putString(BackendConstants.PRODUCT_BARCODE_COLUMN,scannedCode.getText().toString());
            args.putString(BackendConstants.PRODUCT_QUANTITY,quantity.getText().toString());
            newCategoryDialog.setProductSavedListener(this);
            newCategoryDialog.setArguments(args);
            newCategoryDialog.show(getSupportFragmentManager(),"categoryDialog");
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(BackendConstants.PRODUCT_BARCODE_COLUMN,scannedCode.getText().toString());
        outState.putString(BackendConstants.PRODUCT_EXPIRATION,date.getText().toString());
        super.onSaveInstanceState(outState);
    }

    private void clearViews() {
        date.setText(BackendConstants.EMPTY_TEXT_VIEW);
        addFoodController.resetDateValue();
        scannedCode.setText(R.string.enter_code);
        productName.setText(BackendConstants.EMPTY_TEXT_VIEW);
        categoryType.setText(BackendConstants.EMPTY_TEXT_VIEW);
        quantity.setText("1");
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

}
