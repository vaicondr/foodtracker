package cz.cvut.fel.foodtracker.controllers;

import android.app.DatePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import cz.cvut.fel.foodtracker.definitions.UnitType;
import cz.cvut.fel.foodtracker.entities.Fridge;
import cz.cvut.fel.foodtracker.entities.ProductBranded;
import cz.cvut.fel.foodtracker.entities.ProductCategory;
import cz.cvut.fel.foodtracker.entities.ProductType;
import cz.cvut.fel.foodtracker.entities.ProductUnit;
import cz.cvut.fel.foodtracker.services.ifaces.ProductServiceInterface;

/**
 * Created by vaicondr on 3/24/2018.
 */

public class AddFoodController {

    @Inject
    ProductServiceInterface productService;

    long dateValue;

    @Inject
    public AddFoodController() {

    }

    public void addAdapterToProductNameView(Context context,AutoCompleteTextView autoCompleteTextView) {
        List<String> productNames = new LinkedList<>();
        for(ProductType productType : productService.findAllProductTypes()) {
            productNames.add(productType.getProductName());
        }
        createAdapter(context,productNames,autoCompleteTextView);
    }

    public void addAdapterToProductCategoryView(Context context,AutoCompleteTextView autoCompleteTextView) {
        List<String> categoryNames = new LinkedList<>();
        for(ProductCategory productCategory : productService.findAllProductCategories()) {
            categoryNames.add(productCategory.getCategoryName());
        }
        createAdapter(context,categoryNames,autoCompleteTextView);
    }

    public void addAdapterToStorage(Context context,AutoCompleteTextView autoCompleteTextView) {
        List<String> fridgeNames = new LinkedList<>();
        for(Fridge fridge: productService.findAllFridges()) {
            fridgeNames.add(fridge.getFridgeName());
        }
        createAdapter(context,fridgeNames,autoCompleteTextView);
    }

    private void createAdapter(Context context,List<String> hints,AutoCompleteTextView autoCompleteTextView) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(context,
                android.R.layout.simple_dropdown_item_1line,hints.toArray(new String[hints.size()]));
        autoCompleteTextView.setAdapter(adapter);
    }

    public void addProduct(String categoryType,String productName,String scannedCode,
                           String quantity,String storageName,String unitType) {
        ProductCategory productCategory = tryAddingProductCategory(categoryType);
        ProductType productType = tryAddingProductType(productCategory,productName,unitType);
        tryAddingProduct(productType,scannedCode);
        tryAddingFridge(storageName);
        tryAddingProductUnit(productType,quantity,storageName);
    }

    private ProductCategory tryAddingProductCategory(String categoryType) {
        ProductCategory productCategory = productService.findCategoryByName(categoryType);

        if(productCategory == null) {
            productService.saveCategory(new ProductCategory(categoryType,1));
            productCategory = productService
                    .findCategoryByName(categoryType);
        }
        return productCategory;
    }

    private ProductType tryAddingProductType(ProductCategory productCategory,String productName,String unitType) {
        ProductType productType = productService.findTypeByName(productName);
        if(productType == null) {
            productType = new ProductType(productCategory.getId(),productName,unitType);
            productService.saveType(productType);
            productType = productService.findTypeByName(productName);
        } else {
            if(productType.getProductCategoryId() != productCategory.getId()) {
                productType.setProductCategoryId(productCategory.getId());
                productService.updateProductType(productType);
            }

            if(!unitType.equals(productType.getUnits())) {
                productType.setUnits(unitType);
                productService.updateProductType(productType);
            }
        }
        return productType;
    }

    private void tryAddingProduct(ProductType productType, String scannedCode) {
        if(scannedCode.isEmpty()) {
            return;
        }

        ProductBranded product = productService.findBrandedProductByBarcode(scannedCode);

        if(product != null) {
            boolean productTypeChanged = product.getProductTypeId() != productType.getId();
            if(productTypeChanged) {
                ProductType oldProductType = productService.findType(product.getProductTypeId());
                alterProductsAffectedByBarcodeChange(oldProductType,productType);
                product.setProductTypeId(productType.getId());
                productService.updateBranded(product);
            }
        } else {
            product = new ProductBranded(productType.getId(),scannedCode);
            productService.saveBranded(product);
        }

    }

    private void alterProductsAffectedByBarcodeChange(ProductType oldProductType,
                                                      ProductType newProductType) {
            oldProductType.setProductName(newProductType.getProductName());
            List<ProductUnit> productUnits = productService.findAllProductUnits();
            for(ProductUnit productUnit : productUnits) {
                if(productUnit.getProductTypeId() == oldProductType.getId()) {
                    productUnit.setProductTypeId(newProductType.getId());
                    productService.updateProductUnit(productUnit);
                }
            }
            productService.deleteProductType(oldProductType);
            productService.updateProductType(newProductType);
    }

    private void tryAddingFridge(String fridgeName) {
        if(fridgeName.isEmpty()) {
            return;
        }
        Fridge fridge = productService.findFridgeByName(fridgeName);
        if(fridge == null) {
            productService.saveFridge(fridgeName);
        }
    }

    private void tryAddingProductUnit(ProductType productType, String quantity, String storageName) {
        Fridge fridge = productService.findFridgeByName(storageName);
        long fridgeId;
        if(fridge == null) {
            fridgeId = 0;
        } else {
            fridgeId = fridge.getId();
        }

        ProductUnit productUnit = productService
                .findUnitByTypeIdExpirationDateAndStorageId(dateValue,productType.getId(),fridgeId);

        if(productUnit != null) {
            productUnit.setQuantity(productUnit.getQuantity() + Integer.parseInt(quantity));
            productService.deleteProductUnit(productUnit);
        } else {
            productUnit = new ProductUnit(Integer.parseInt(quantity),dateValue,fridgeId, productType.getId());
        }
        productService.saveProductUnit(productUnit);
    }

    public void tryToPrefillDataBasedOnScannedCode(String possibleProductId, EditText productName,
                                                   EditText categoryType, Spinner unitsSpinner) {


        ProductBranded product = productService.findBrandedProductByBarcode(possibleProductId);
        if(product != null) {
            ProductType productType = productService.findType(product.getProductTypeId());
            ProductCategory productCategory = productService.findCategory(productType.getProductCategoryId());

            productName.setText(productType.getProductName());
            String unitType = productType.getUnits();
            for (int i = 0; i < UnitType.values().length; i++) {
                if(unitType.equals(UnitType.values()[i].toString())) {
                    unitsSpinner.setSelection(i);
                    continue;
                }
            }

            categoryType.setText(productCategory.getCategoryName());
        }

    }

    public void addListenerToSelectDate(final Context context, Button selectDate, final TextView date) {
        final Calendar calendar = Calendar.getInstance();
        final int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        final int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        selectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                Calendar calendar = new GregorianCalendar(year,month,day);
                                date.setText(day + "/" + ++month + "/" + year);
                                dateValue = calendar.getTimeInMillis();
                            }
                        }, year, month, dayOfMonth);
                datePickerDialog.show();
            }
        });
    }

    public void addListenerToProductName(final AutoCompleteTextView productName,
                                         final AutoCompleteTextView productCategoryView, final Spinner unitSpinner) {
        productName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                tryToPrefillDataBasedOnProductName(productName.getText().toString(),productCategoryView,unitSpinner);
            }
        });
    }

    public void tryToPrefillDataBasedOnProductName(String productName, EditText productCategoryView,Spinner unitsSpinner) {
        ProductType productType = productService.findTypeByName(productName);
        if(productType != null) {
            String unitType = productType.getUnits();
            for (int i = 0; i < UnitType.values().length; i++) {
                if(unitType.equals(UnitType.values()[i].toString())) {
                    unitsSpinner.setSelection(i);
                    continue;
                }
            }

            ProductCategory productCategory = productService.findCategory(productType.getProductCategoryId());
            if(productCategory != null) {
                productCategoryView.setText(productCategory.getCategoryName());
            }
        }
    }


    public String findLargestStorage() {
        Fridge largestFridge = productService.findLargestFridge();
        if(largestFridge != null) {
            return largestFridge.getFridgeName();
        } else {
            return "";
        }
    }

    public boolean categoryExists(String category) {
        ProductCategory productCategory = productService.findCategoryByName(category);
        return productCategory != null;
    }

    public boolean storageExists(String storage) {
        Fridge fridge = productService.findFridgeByName(storage);
        return fridge != null;
    }


    public long getDateValue() {
        return dateValue;
    }

    public void setDateValue(long dateValue) {
        this.dateValue = dateValue;
    }

    public void resetDateValue() {
        dateValue = 0;
    }
}
